//
//  PYHeader.h
//  Pods
//
//  Created by yunhe.lin on 16/7/26.
//
//

#ifndef PYHeader_h
#define PYHeader_h
//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=APPID"]];

#import "PYCategoryHeader.h"
#import "PYDateFormatterHelper.h"
#import "PYUserDefaults.h"
#import "PYApplicationHelper.h"
//TODO: 宏定义
#if DEBUG
#define PYJSONLog(...) NSLog(__VA_ARGS__)
#else
#define PYJSONLog(...)
#endif

#ifdef DEBUG
#   define PYLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define PYLog(...)
#endif

////通过NSNumberFormatter，同样可以设置NSNumber输出的格式。例如如下代码：
//NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//formatter.numberStyle = NSNumberFormatterDecimalStyle;
//NSString *string = [formatter stringFromNumber:[NSNumber numberWithInt:123456789]];
//NSLog(@"Formatted number string:%@",string);
////输出结果为：[1223:403] Formatted number string:123,456,789
//
////其中NSNumberFormatter类有个属性numberStyle，它是一个枚举型，设置不同的值可以输出不同的数字格式。该枚举包括：
//typedef NS_ENUM(NSUInteger, NSNumberFormatterStyle) {
//    NSNumberFormatterNoStyle = kCFNumberFormatterNoStyle,
//    NSNumberFormatterDecimalStyle = kCFNumberFormatterDecimalStyle,
//    NSNumberFormatterCurrencyStyle = kCFNumberFormatterCurrencyStyle,
//    NSNumberFormatterPercentStyle = kCFNumberFormatterPercentStyle,
//    NSNumberFormatterScientificStyle = kCFNumberFormatterScientificStyle,
//    NSNumberFormatterSpellOutStyle = kCFNumberFormatterSpellOutStyle
//};
////各个枚举对应输出数字格式的效果如下：其中第三项和最后一项的输出会根据系统设置的语言区域的不同而不同。
//[1243:403] Formatted number string:123456789
//[1243:403] Formatted number string:123,456,789
//[1243:403] Formatted number string:￥123,456,789.00
//[1243:403] Formatted number string:-539,222,988%
//[1243:403] Formatted number string:1.23456789E8
//[1243:403] Formatted number string:一亿二千三百四十五万六千七百八十九

//TODO: 如何获取WebView所有的图片地址，
//
//在网页加载完成时，通过js获取图片和添加点击的识别方式
//
////UIWebView
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    //这里是js，主要目的实现对url的获取
//    static  NSString * const jsGetImages =
//    @"function getImages(){\
//    var objs = document.getElementsByTagName(\"img\");\
//    var imgScr = '';\
//    for(var i=0;i<objs.length;i++){\
//    imgScr = imgScr + objs[i].src + '+';\
//    };\
//    return imgScr;\
//    };";
//    
//    [webView stringByEvaluatingJavaScriptFromString:jsGetImages];//注入js方法
//    NSString *urlResult = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
//    NSArray *urlArray = [NSMutableArray arrayWithArray:[urlResult componentsSeparatedByString:@"+"]];
//    //urlResurlt 就是获取到得所有图片的url的拼接；mUrlArray就是所有Url的数组
//}
////WKWebView
//- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
//{
//    static  NSString * const jsGetImages =
//    @"function getImages(){\
//    var objs = document.getElementsByTagName(\"img\");\
//    var imgScr = '';\
//    for(var i=0;i<objs.length;i++){\
//    imgScr = imgScr + objs[i].src + '+';\
//    };\
//    return imgScr;\
//    };";
//    
//    [webView evaluateJavaScript:jsGetImages completionHandler:nil];
//    [webView evaluateJavaScript:@"getImages()" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        NSLog(@"%@",result);
//    }];
//}
//获取到webview的高度
//CGFloat height = [[self.webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue];
//
//文／iOS_啼血无痕_小松哥（简书作者）
//原文链接：http://www.jianshu.com/p/4523eafb4cd4
//著作权归作者所有，转载请联系作者获得授权，并标注“简书作者”。

#endif /* PYHeader_h */
