//
//  PYUserDefaults.m
//  Pods
//
//  Created by yunhe.lin on 16/9/7.
//
//

#import "PYUserDefaults.h"

static const NSUserDefaults *userDefault = nil;

@interface PYUserDefaults()

@property (nonatomic, strong) NSUserDefaults *userDefaults;

@end

@implementation PYUserDefaults

+ (BOOL)setObject:(id)object forKey:(NSString *)key
{
    [self configUserDefault];
    if (!object || key.length == 0) {
        return NO;
    }
    [userDefault setObject:object forKey:key];
    return [userDefault synchronize];
}

+ (id)objectForKey:(NSString *)key
{
    [self configUserDefault];
    if (key.length > 0) {
        return [userDefault objectForKey:key];
    }
    return nil;
}

+ (void)removeObjectForKey:(NSString *)key
{
    [self configUserDefault];
    if (key.length == 0) {
        return;
    }
    [userDefault removeObjectForKey:key];
}

+ (void)removeAllObject
{
    [self configUserDefault];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [userDefault removePersistentDomainForName:appDomain];
}

+ (void)configUserDefault
{
    if (!userDefault) {
        userDefault = [NSUserDefaults standardUserDefaults];
    }
}

@end
