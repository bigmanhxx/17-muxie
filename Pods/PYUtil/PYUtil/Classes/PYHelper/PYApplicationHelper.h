//
//  PYApplicationHelper.h
//  Pods
//
//  Created by yunhe.lin on 16/9/7.
//
//

#import <Foundation/Foundation.h>

@interface PYApplicationHelper : NSObject

+ (void)setStatusBarBackgroundColor:(UIColor *)color;

@end
