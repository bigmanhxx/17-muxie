//
//  PYApplicationHelper.m
//  Pods
//
//  Created by yunhe.lin on 16/9/7.
//
//

#import "PYApplicationHelper.h"

@implementation PYApplicationHelper

+ (void)setStatusBarBackgroundColor:(UIColor *)color
{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)])
    {
        statusBar.backgroundColor = color;
    }
}

@end
