//
//  PYUserDefaults.h
//  Pods
//
//  Created by yunhe.lin on 16/9/7.
//
//

#import <Foundation/Foundation.h>

@interface PYUserDefaults : NSObject

+ (BOOL)setObject:(id)object forKey:(NSString *)key;

+ (id)objectForKey:(NSString *)key;

+ (void)removeObjectForKey:(NSString *)key;

+ (void)removeAllObject;

@end
