//
//  BZGetCodeRequest.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZGetCodeRequest.h"

@implementation BZGetCodeRequest

//1）	接口名称： /app/sms/code
//2）	method：post
//3）	请求参数：
//1、	telphone ：手机号码
//2、	version：0.1（固定值）
//3、	token：（2个小时有效）
//4）	HTTP状态码：200
//5）	返回参数：无

- (NSString *)methodName
{
    return @"/app/sms/code";
}





@end
