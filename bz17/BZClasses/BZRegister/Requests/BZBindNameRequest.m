//
//  BZBindNameRequest.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBindNameRequest.h"
#import "BZNetworkHelper.h"
#import "BZConfigManager.h"
#import "HYBNetworking.h"

typedef void(^RequestBlock)(void);

@implementation BZBindNameRequest

//1）	接口名称：/app/bind/nickname
//2）	method：post
//3）	请求参数：
//1、	nickname：昵称
//2、	headimg：byte［］头像
//3、	version：0.1（固定值）
//4、	key：
//4）	HTTP状态码：200
//5）	返回参数：
//1、key：登录信息

- (NSString *)methodName
{
    return @"/app/bind/nickname";
}

- (void)sendRequest
{
    RequestBlock requestBlock = ^(void) {
        NSString *url = [self url];
        if (!url.length) {
            return;
        }
        [self.requestParam addEntriesFromDictionary:@{
                                                      @"version": @"0.1"
                                                      }];
        UIImage *image = [UIImage imageWithData:self.requestParam[@"headimg"]];
        [self.requestParam removeObjectForKey:@"headimg"];
        if (image) {
            [HYBNetworking uploadWithImage:image url:url filename:@"headimg" name:@"headimg" mimeType:@"application/json;charset=UTF-8" parameters:self.requestParam progress:^(int64_t bytesWritten, int64_t totalBytesWritten) {
                
            } success:^(id response) {
                BLOCK_EXEC(self.completeBlock,response);
            } fail:^(NSError *error) {
                BLOCK_EXEC(self.errorBlock,error);
            }];
        } else {
            [self.requestParam removeObjectForKey:@"headimg"];
            [super sendRequest];
        }
    };
    if ([[self methodName] isEqualToString:@"/app/token"]) {
        requestBlock();
    } else {
        [BZConfigManager requestToken:^(NSString *token) {
            if (!token.length) {
                self.errorBlock([self tokenError]);
            } else {
                [self.requestParam addEntriesFromDictionary:@{
                                                              @"token": token
                                                              }];
                requestBlock();
            }
        }];
    }
}

- (NSError *)tokenError
{
    NSError *tokenError = [[NSError alloc] initWithDomain:@"token 获取失败" code:101 userInfo:nil];
    return tokenError;
}

@end
