//
//  BZModifyPhoneRequest.m
//  bz17
//
//  Created by yunhe.lin on 16/7/18.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZModifyPhoneRequest.h"

//1、	telphone：手机号
//2、	code：验证码
//3、	mid：授权中间关键值
//4、	verion：0.1（固定值）
//5、	token：


@implementation BZModifyPhoneRequest

- (NSString *)methodName
{
    return @"/app/modify/phone";
}

@end
