//
//  BZSinaBindRequest.m
//  bz17
//
//  Created by yunhe.lin on 16/7/19.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZSinaBindRequest.h"

@implementation BZSinaBindRequest

- (NSString *)methodName
{
    return @"/app/oauth/webo";
}

- (void)sendRequest
{
    self.requestParam[@"r"] = @"001";
    self.requestParam[@"c"] = @"ios";
    [super sendRequest];
}

@end
