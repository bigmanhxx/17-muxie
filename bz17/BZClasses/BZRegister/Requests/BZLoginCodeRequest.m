//
//  BZLoginCodeRequest.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZLoginCodeRequest.h"

@implementation BZLoginCodeRequest

//1）	接口名称：/app/login
//2）	method：post
//3）	请求参数：
//1、	telphone：手机号码
//2、	code：验证码
//3、	r：推荐码
//4、	c：渠道代码
//5、	version：0.1（固定值）
//6、	token：（2个小时有效）
//4）	HTTP状态码：200
//5）	返回参数：
//1、key：登录信息

- (NSString *)methodName
{
    return @"/app/login";
}


@end
