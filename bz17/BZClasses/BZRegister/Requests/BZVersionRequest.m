//
//  BZVersionRequest.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZVersionRequest.h"

@implementation BZVersionRequest

- (NSString *)methodName
{
    return @"/app/update";
}

@end
