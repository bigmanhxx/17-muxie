//
//  BZTokenRequest.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTokenRequest.h"

@implementation BZTokenRequest

- (NSMutableDictionary *)requestParam
{
    
    return [NSMutableDictionary dictionaryWithDictionary:@{
                                                           @"version": @"0.1"
                                                           }];
}

- (NSString *)methodName
{
    return @"/app/token";
}
@end
