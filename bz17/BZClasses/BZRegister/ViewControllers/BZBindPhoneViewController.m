//
//  BZBindPhoneViewController.m
//  bz17
//
//  Created by bigMan on 16/6/30.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBindPhoneViewController.h"
#import "BZThirdUserInfo.h"
#import "BZGetCodeRequest.h"
#import "BZBindNameRequest.h"
#import "BZModifyPhoneRequest.h"

static CGFloat kHeadImageViewHeight = 70.f;
static CGFloat padding = 15.f;
static CGFloat textViewHeight = 197.f / 2.0;
static CGFloat textFieldHeight = 25.f;
static NSInteger codeCount = 60;

@interface BZBindPhoneViewController()

@property (nonatomic, strong) BZThirdUserInfo *userInfo;
@property (nonatomic, strong) UIImageView *userHeadImageView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIView      *textBackView;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton    *codeButton;
@property (nonatomic, strong) NSTimer     *codeTimer;
@property (nonatomic, assign) NSInteger   timeCount;;
@property (nonatomic, assign) CGFloat     textLeft;

@property (nonatomic, strong) UIButton    *bindButton;

@property (nonatomic, strong) BZGetCodeRequest *codeReuqest;
@property (nonatomic, strong) BZModifyPhoneRequest *modifyPhoneRequest;

@end

@implementation BZBindPhoneViewController

- (void)setParam:(NSDictionary *)param
{
    self.userInfo = param[@"userInfo"];
    [self.userHeadImageView sd_setImageWithURL:[NSURL URLWithString:self.userInfo.avatar] placeholderImage:BZDefaultImage];
    self.nameLabel.text = self.userInfo.nickname;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    //注册键盘消失的通知
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)initView
{
    self.title = @"手机号绑定";
    self.view.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
    [self.view addSubview:self.userHeadImageView];
    [self.userHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(45.f);
        make.centerX.equalTo(self.view);
        make.width.height.equalTo(@(kHeadImageViewHeight));
    }];
    
    [self.view addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.userHeadImageView);
        make.top.equalTo(self.userHeadImageView.mas_bottom).offset(10.f);
    }];
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"为了使用顺畅，请绑定您的手机号";
    tipLabel.font = BZFont(12.f);
    tipLabel.textColor = [BZSystemManager colorWithHex:0xffa003];
    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(20.f);
        make.centerX.equalTo(self.view);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"bz_login_tip"];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(tipLabel.mas_left);
        make.centerY.equalTo(tipLabel);
    }];
    
    [self.view addSubview:self.textBackView];
    [self.textBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.view);
        make.top.equalTo(tipLabel.mas_bottom).offset(15.f);
        make.height.equalTo(@(textViewHeight));
    }];
    
    {
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, textViewHeight / 2.0)];
        phoneLabel.text = @"手机号码";
        phoneLabel.textColor = [BZSystemManager colorWithHex:0xc6c6c6];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.font = [UIFont systemFontOfSize:14.f];
        CGSize textSize = [phoneLabel.text py_sizeForFont:phoneLabel.font contentSize:CGSizeMake(1010, 1010)];
        phoneLabel.frame = CGRectMake(padding, 0, textSize.width, phoneLabel.viewHeight);
        [self.textBackView addSubview:phoneLabel];
        self.textLeft = (phoneLabel.right + 19.f);
    }
    {
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, textViewHeight / 2.0)];
        phoneLabel.text = @"验证码";
        phoneLabel.textColor = [BZSystemManager colorWithHex:0xc6c6c6];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.font = BZFont(14.f);
        CGSize textSize = [phoneLabel.text py_sizeForFont:phoneLabel.font contentSize:CGSizeMake(1010, 1010)];
        phoneLabel.frame = CGRectMake(padding, textViewHeight / 2.0, textSize.width, phoneLabel.viewHeight);
        [self.textBackView addSubview:phoneLabel];
    }
    [self.textBackView addSubview:self.phoneTextField];
    [self.textBackView addSubview:self.codeTextField];
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [BZSystemManager colorWithHex:0xdddddd];
    [self.textBackView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@1);
        make.left.equalTo(self.codeTextField.mas_right);
        make.height.equalTo(self.codeTextField.mas_height).offset(10);
        make.centerY.equalTo(self.codeTextField);
    }];
    [self.textBackView addSubview:self.codeButton];
    [self.codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView.mas_right);
        make.top.equalTo(self.codeTextField);
        make.bottom.equalTo(self.codeTextField);
        make.right.equalTo(self.textBackView);
    }];
    
    [self.view addSubview:self.bindButton];
    [self.bindButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textBackView.mas_bottom).offset(22.f);
        make.left.equalTo(self.view).mas_offset(12.f);
        make.right.equalTo(self.view).offset(-12.f);
        make.height.equalTo(@48.f);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - private method

- (void)getPhoneCode
{
    if (!self.phoneTextField.text.length) {
        [self showSimpleHUD:@"输入手机号码" afterDelay:1.0];
        return;
    }
    
    if (![BZSystemManager validateMobile:self.phoneTextField.text]) {
        [self showSimpleHUD:@"输入正确手机号码" afterDelay:1.0];
        return;
    }
    [self.codeReuqest.requestParam addEntriesFromDictionary:@{
                                                              @"telphone": self.phoneTextField.text
                                                              }];
    [self showHUB];
    [self.codeReuqest sendRequest];
}

- (void)successGetCode:(id)code
{
    [self hiddenHUB];
    [self showSimpleHUD:@"获取验证码成功" afterDelay:1.0];
    self.timeCount  = codeCount;
    [self.codeButton setTitle:[NSString stringWithFormat:@"%zd秒后重发",self.timeCount] forState:UIControlStateDisabled];
    self.codeButton.enabled = NO;
    self.codeTimer = nil;
    [[NSRunLoop currentRunLoop] addTimer:self.codeTimer forMode:NSRunLoopCommonModes];
}

- (void)errorGetCode
{
    [self hiddenHUB];
    [self showSimpleHUD:@"获取验证码失败" afterDelay:1.0];
}

- (void)codeCountDown
{
    [self.codeButton setTitle:[NSString stringWithFormat:@"%zd秒后重发",self.timeCount] forState:UIControlStateDisabled];
    self.timeCount--;
    if (self.timeCount == 0) {
        [self resetCodeButton];
    }
}

- (void)resetCodeButton
{
    self.codeButton.enabled = YES;
    [self.codeTimer invalidate];
}

- (void)bindingAction
{
    if (!self.phoneTextField.text.length) {
        [self showSimpleHUD:@"输入手机号码" afterDelay:1.0];
        return;
    }
    
    if (![BZSystemManager validateMobile:self.phoneTextField.text]) {
        [self showSimpleHUD:@"输入正确手机号码" afterDelay:1.0];
        return;
    }
    
    if (!self.codeTextField.text.length) {
        [self showSimpleHUD:@"输入验证码" afterDelay:1.0];
        return;
    }
    [self.modifyPhoneRequest.requestParam addEntriesFromDictionary:@{
                                                                     @"code": self.codeTextField.text,
                                                                     @"telphone": self.phoneTextField.text,
                                                                     @"mid": self.userInfo.mid
                                                                     }];
    [self showHUB];
    [self.modifyPhoneRequest sendRequest];
}

- (void)thirdLoginSuccess:(NSDictionary *)result
{
    [self hiddenHUB];
    [BZSystemManager updatePersonKey:result[@"key"]];
    [BZSystemManager popToRootViewController];
}

- (void)thirdLoginError:(NSError *)result
{
    [self hiddenHUB];
    [self showSimpleHUD:result.domain afterDelay:1.0];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = self.view.viewHeight - self.textBackView.bottom - keyBoardFrame.size.height;
    if (height > 0) {
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, height, self.view.viewWidth, self.view.viewHeight)];
    }];
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, 64.f, self.view.viewWidth, self.view.viewHeight)];
    }];
}

#pragma mark - accessors method 

- (UIImageView *)userHeadImageView {
    if(_userHeadImageView == nil) {
        _userHeadImageView = [[UIImageView alloc] init];
        _userHeadImageView.image = [UIImage imageNamed:@"default_user_head"];
        _userHeadImageView.userInteractionEnabled = YES;
        _userHeadImageView.layer.cornerRadius = kHeadImageViewHeight / 2.0;
        _userHeadImageView.layer.masksToBounds = YES;
    }
    return _userHeadImageView;
}

- (UILabel *)nameLabel {
	if(_nameLabel == nil) {
		_nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [BZSystemManager colorWithHex:0x333333];
        _nameLabel.font = BZFont(12.f);
        _nameLabel.textAlignment = NSTextAlignmentCenter;
	}
	return _nameLabel;
}

- (UIView *)textBackView {
    if(_textBackView == nil) {
        _textBackView = [[UIView alloc] init];
        _textBackView.backgroundColor = [UIColor whiteColor];
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [BZSystemManager colorWithHex:0xdddddd];
        [_textBackView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_textBackView);
            make.right.equalTo(_textBackView);
            make.left.equalTo(_textBackView).offset(padding);
            make.height.equalTo(@1);
        }];
    }
    return _textBackView;
}

- (UITextField *)phoneTextField {
    if(_phoneTextField == nil) {
        _phoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.textLeft, (textViewHeight / 2.0 - textFieldHeight) / 2.0 + 1.5f, SCREEN_WIDTH - self.textLeft, textFieldHeight)];
        _phoneTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"输入手机号码" attributes:@{
                                                                                                                 NSForegroundColorAttributeName:[BZSystemManager colorWithHex:0xdddddd]
                                                                                                                 }];
        _phoneTextField.textColor = [BZSystemManager colorWithHex:0x333333];
        _phoneTextField.font = BZFont(14.f);
        _phoneTextField.textAlignment = NSTextAlignmentLeft;
//        _phoneTextField.delegate = self;
    }
    return _phoneTextField;
}

- (UITextField *)codeTextField {
    if(_codeTextField == nil) {
        _codeTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.textLeft, self.phoneTextField.bottom + textViewHeight /4.0, SCREEN_WIDTH - 2*self.textLeft, textFieldHeight)];
        _codeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{
                                                                                                                  NSForegroundColorAttributeName:[BZSystemManager colorWithHex:0xdddddd]
                                                                                                                  }];
        _codeTextField.textColor = [BZSystemManager colorWithHex:0x333333];
        _codeTextField.font = BZFont(14.f);
        _codeTextField.textAlignment = NSTextAlignmentLeft;
    }
    return _codeTextField;
}

- (UIButton *)codeButton {
    if(_codeButton == nil) {
        _codeButton = [[UIButton alloc] init];
        _codeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_codeButton setTitleColor:[BZSystemManager colorWithHex:0xff6a00] forState:UIControlStateNormal];
        [_codeButton setTitleColor:[BZSystemManager colorWithHex:0xc7c7c7] forState:UIControlStateDisabled];
        [_codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        _codeButton.titleLabel.font = BZFont(14.f);
        [_codeButton addTarget:self action:@selector(getPhoneCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeButton;
}

- (NSTimer *)codeTimer {
    if(_codeTimer == nil) {
        _codeTimer = [NSTimer timerWithTimeInterval:1.f target:self selector:@selector(codeCountDown) userInfo:nil repeats:YES];
    }
    return _codeTimer;
}

- (UIButton *)bindButton {
	if(_bindButton == nil) {
		_bindButton = [[UIButton alloc] init];
        [_bindButton setTitle:@"绑 定" forState:UIControlStateNormal];
        [_bindButton setBackgroundColor:[BZSystemManager colorWithHex:0xff6a00]];
        [_bindButton addTarget:self action:@selector(bindingAction) forControlEvents:UIControlEventTouchUpInside];
	}
	return _bindButton;
}

- (BZGetCodeRequest *)codeReuqest {
    if(_codeReuqest == nil) {
        _codeReuqest = [[BZGetCodeRequest alloc] init];
        @weakify(self);
        _codeReuqest.completeBlock = ^(id result) {
            @strongify(self);
            [self successGetCode:result];
        };
        
        _codeReuqest.errorBlock = ^(id error) {
            @strongify(self);
            [self errorGetCode];
        };
    }
    return _codeReuqest;
}

- (BZModifyPhoneRequest *)modifyPhoneRequest {
	if(_modifyPhoneRequest == nil) {
		_modifyPhoneRequest = [[BZModifyPhoneRequest alloc] init];
        @weakify(self);
        _modifyPhoneRequest.completeBlock = ^(id result) {
            @strongify(self);
            [self thirdLoginSuccess:result];
        };
        
        _modifyPhoneRequest.errorBlock = ^(id error) {
            @strongify(self);
            [self thirdLoginError:error];
        };
	}
	return _modifyPhoneRequest;
}


@end
