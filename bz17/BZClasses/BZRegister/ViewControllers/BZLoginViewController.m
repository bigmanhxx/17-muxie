//
//  BZLoginViewController.m
//  bz17
//
//  Created by bigMan on 16/6/29.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZLoginViewController.h"
#import "BZThirdLoginView.h"
#import "BZThirdUserInfo.h"
#import "BZPopWindowHelper.h"
#import "BZShareManager.h"
#import "BZGetCodeRequest.h"
#import "BZLoginCodeRequest.h"

static CGFloat padding = 15.f;
static CGFloat textViewHeight = 197.f / 2.0;
static CGFloat textFieldHeight = 25.f;
static NSInteger codeCount = 60;


@interface BZLoginViewController()<UITextFieldDelegate>

@property (nonatomic, strong) UIView      *textBackView;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton    *codeButton;
@property (nonatomic, strong) UIButton    *loginButton;
@property (nonatomic, strong) BZThirdLoginView *thirdLoginView;
@property (nonatomic, strong) NSTimer     *codeTimer;
@property (nonatomic, assign) NSInteger   timeCount;;
@property (nonatomic, assign) CGFloat     textLeft;
@property (nonatomic, strong) BZGetCodeRequest *codeReuqest;
@property (nonatomic, strong) BZLoginCodeRequest *loginCodeRequest;

@end

@implementation BZLoginViewController

- (void)setParam:(NSDictionary *)param
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    self.title = @"登录";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [[BZPopWindowHelper shareInstance] showPopWindow:BZPopWindowTypeShare withBlock:^(id param) {
//        NSNumber *type = param;
//        [[BZShareManager shareInstance] shareParam:@{
//                                                     @"text": @"test",
//                                                     @"url": [NSURL URLWithString:@"www.baidu.com"],
//                                                     @"title": @"title"
//                                                     }
//                                          platform:type.integerValue
//                                       resultShare:^(SSDKResponseState responseState) {
//                                       
//                                       }];
//    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.timeCount = 1;
    [self codeCountDown];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)initView
{
    self.view.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
    [self.view addSubview:self.textBackView];
    {
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, self.textBackView.viewHeight / 2.0)];
        phoneLabel.text = @"手机号码";
        phoneLabel.textColor = [BZSystemManager colorWithHex:0xc6c6c6];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.font = [UIFont systemFontOfSize:14.f];
        CGSize textSize = [phoneLabel.text py_sizeForFont:phoneLabel.font contentSize:CGSizeMake(1010, 1010)];
        phoneLabel.frame = CGRectMake(padding, 0, textSize.width, phoneLabel.viewHeight);
        [self.textBackView addSubview:phoneLabel];
        self.textLeft = (phoneLabel.right + 19.f);
    }
    {
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, self.textBackView.viewHeight / 2.0)];
        phoneLabel.text = @"验证码";
        phoneLabel.textColor = [BZSystemManager colorWithHex:0xc6c6c6];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.font = BZFont(14.f);
        CGSize textSize = [phoneLabel.text py_sizeForFont:phoneLabel.font contentSize:CGSizeMake(1010, 1010)];
        phoneLabel.frame = CGRectMake(padding, self.textBackView.viewHeight / 2.0, textSize.width, phoneLabel.viewHeight);
        [self.textBackView addSubview:phoneLabel];
    }
    [self.textBackView addSubview:self.phoneTextField];
    [self.textBackView addSubview:self.codeTextField];
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [BZSystemManager colorWithHex:0xdddddd];
    [self.textBackView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@1);
        make.left.equalTo(self.codeTextField.mas_right);
        make.height.equalTo(self.codeTextField.mas_height).offset(10);
        make.centerY.equalTo(self.codeTextField);
    }];
    [self.textBackView addSubview:self.codeButton];
    [self.codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView.mas_right);
        make.top.equalTo(self.codeTextField);
        make.bottom.equalTo(self.codeTextField);
        make.right.equalTo(self.textBackView);
    }];
    [self.view addSubview:self.loginButton];
    [self.view addSubview:self.thirdLoginView];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - private method 

- (void)getPhoneCode
{
    if (!self.phoneTextField.text.length) {
        [self showSimpleHUD:@"输入手机号码" afterDelay:1.0];
        return;
    }
    
    if (![BZSystemManager validateMobile:self.phoneTextField.text]) {
        [self showSimpleHUD:@"输入正确手机号码" afterDelay:1.0];
        return;
    }
    [self showHUB];
    [self.codeReuqest.requestParam addEntriesFromDictionary:@{
                                                              @"telphone": self.phoneTextField.text
                                                              }];
    [self.codeReuqest sendRequest];
}

- (void)successGetCode:(id)code
{
    [self hiddenHUB];
    [self showSimpleHUD:@"获取验证码成功" afterDelay:1.0];
    self.timeCount  = codeCount;
    [self.codeButton setTitle:[NSString stringWithFormat:@"%zd秒后重发",self.timeCount] forState:UIControlStateDisabled];
    self.codeButton.enabled = NO;
    self.codeTimer = nil;
    [[NSRunLoop currentRunLoop] addTimer:self.codeTimer forMode:NSRunLoopCommonModes];
}

- (void)errorGetCode
{
    [self hiddenHUB];
    [self showSimpleHUD:@"获取验证码失败" afterDelay:1.0];
}

- (void)codeCountDown
{
    [self.codeButton setTitle:[NSString stringWithFormat:@"%zd秒后重发",self.timeCount] forState:UIControlStateDisabled];
    self.timeCount--;
    if (self.timeCount == 0) {
        [self resetCodeButton];
    }
}

- (void)resetCodeButton
{
    self.codeButton.enabled = YES;
    [self.codeTimer invalidate];
}

- (void)loginAction
{
    if (!self.phoneTextField.text.length) {
        [self showSimpleHUD:@"输入手机号码" afterDelay:1.0];
        return;
    }
    
    if (![BZSystemManager validateMobile:self.phoneTextField.text]) {
        [self showSimpleHUD:@"输入正确手机号码" afterDelay:1.0];
        return;
    }

    if (!self.codeTextField.text.length) {
        [self showSimpleHUD:@"输入验证码" afterDelay:1.0];
        return;
    }
    [self.loginCodeRequest.requestParam addEntriesFromDictionary:@{
                                                                   @"telphone": self.phoneTextField.text,
                                                                   @"code": self.codeTextField.text,
                                                                   @"r": @"001",
                                                                   @"c": @"ios"
                                                                   }];
    [self showHUB];
    [self.loginCodeRequest sendRequest];
}

- (void)loginSuccess:(NSDictionary *)result
{
    [self hiddenHUB];
    NSNumber *hasImg = result[@"hasImg"];
    if (hasImg.boolValue) {
        [BZSystemManager updatePersonKey:result[@"key"]];
        [self popViewController];
        return;
    }
    [BZSystemManager pushViewController:@"BZLoginSuccessViewController" With:@{
                                                                               @"phoneNum": [self replacePhoneParamter],
                                                                               @"key": result[@"key"]
                                                                               }animated:YES];
}

- (void)loginError
{
    [self hiddenHUB];
    [self showSimpleHUD:@"登录失败" afterDelay:1.0];
}

- (void)thirdLoginAction:(id)userInfo
{
    if ([userInfo isKindOfClass:[BZThirdUserInfo class]]) {
        [BZSystemManager pushViewController:@"BZBindPhoneViewController" With:@{
                                                                                @"userInfo": userInfo
                                                                                }animated:YES];
        return;
    }
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = self.view.viewHeight - self.textBackView.bottom - keyBoardFrame.size.height;
    if (height > 0) {
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, height, self.view.viewWidth, self.view.viewHeight)];
    }];
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, 64.f, self.view.viewWidth, self.view.viewHeight)];
    }];
}

- (NSString *)replacePhoneParamter
{
    NSMutableString *mdPhone = [[NSMutableString alloc] initWithString:self.phoneTextField.text];
    [mdPhone replaceCharactersInRange:NSMakeRange(3, 5) withString:@"*****"];
    return mdPhone;
}

#pragma mark - accessors method

- (UIView *)textBackView {
	if(_textBackView == nil) {
		_textBackView = [[UIView alloc] initWithFrame:CGRectMake(0, padding, SCREEN_WIDTH, textViewHeight)];
        _textBackView.backgroundColor = [UIColor whiteColor];
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [BZSystemManager colorWithHex:0xdddddd];
        [_textBackView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_textBackView);
            make.right.equalTo(_textBackView);
            make.left.equalTo(_textBackView).offset(padding);
            make.height.equalTo(@1);
        }];
	}
	return _textBackView;
}

- (UITextField *)phoneTextField {
	if(_phoneTextField == nil) {
		_phoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.textLeft, (textViewHeight / 2.0 - textFieldHeight) / 2.0 + 1.5f, SCREEN_WIDTH - self.textLeft, textFieldHeight)];
        _phoneTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"输入手机号码" attributes:@{
                                                                                                                 NSForegroundColorAttributeName:[BZSystemManager colorWithHex:0xdddddd]
                                                                                                                 }];
        _phoneTextField.textColor = [BZSystemManager colorWithHex:0x333333];

        _phoneTextField.font = BZFont(14.f);
        _phoneTextField.textAlignment = NSTextAlignmentLeft;
        _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
	}
	return _phoneTextField;
}

- (UITextField *)codeTextField {
	if(_codeTextField == nil) {
		_codeTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.textLeft, self.phoneTextField.bottom + textViewHeight /4.0, SCREEN_WIDTH - 2*self.textLeft, textFieldHeight)];
        _codeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{
                                                                                                                 NSForegroundColorAttributeName:[BZSystemManager colorWithHex:0xdddddd]
                                                                                                                 }];
        _codeTextField.textColor = [BZSystemManager colorWithHex:0x333333];
        _codeTextField.font = BZFont(14.f);
        _codeTextField.textAlignment = NSTextAlignmentLeft;
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
	}
	return _codeTextField;
}

- (UIButton *)codeButton {
	if(_codeButton == nil) {
		_codeButton = [[UIButton alloc] init];
        _codeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_codeButton setTitleColor:[BZSystemManager colorWithHex:0xff6a00] forState:UIControlStateNormal];
        [_codeButton setTitleColor:[BZSystemManager colorWithHex:0xc7c7c7] forState:UIControlStateDisabled];
        [_codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        _codeButton.titleLabel.font = BZFont(14.f);
        [_codeButton addTarget:self action:@selector(getPhoneCode) forControlEvents:UIControlEventTouchUpInside];
	}
	return _codeButton;
}

- (UIButton *)loginButton {
	if(_loginButton == nil) {
		_loginButton = [[UIButton alloc] initWithFrame:CGRectMake(12.f, self.textBackView.bottom + 25.f, SCREEN_WIDTH  -24.f, 48.f)];
        [_loginButton setTitle:@"登 录" forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:[BZSystemManager colorWithHex:0xff6a00]];
        [_loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
	}
	return _loginButton;
}

- (BZThirdLoginView *)thirdLoginView
{
    if(_thirdLoginView == nil) {
        _thirdLoginView = [[BZThirdLoginView alloc] initWithFrame:CGRectMake(0, self.loginButton.bottom + 60.f, SCREEN_WIDTH, SCREEN_HEIGHT - 64.f - self.loginButton.bottom - 60)];
        @weakify(self);
        _thirdLoginView.shareResultBlock = ^(id userInfo) {
            @strongify(self);
            [self hiddenHUB];
            if (![userInfo isKindOfClass:[BZThirdUserInfo class]]) {
                [BZSystemManager popToRootViewController];
                return ;
            }
            [self thirdLoginAction:userInfo];
        };
        _thirdLoginView.requestSendBlock = ^(void){
            @strongify(self);
            [self showHUB];
        };
    }
    return _thirdLoginView;
}

- (NSTimer *)codeTimer {
	if(_codeTimer == nil) {
        _codeTimer = [NSTimer timerWithTimeInterval:1.f target:self selector:@selector(codeCountDown) userInfo:nil repeats:YES];
	}
	return _codeTimer;
}

- (BZGetCodeRequest *)codeReuqest {
	if(_codeReuqest == nil) {
		_codeReuqest = [[BZGetCodeRequest alloc] init];
        @weakify(self);
        _codeReuqest.completeBlock = ^(id result) {
            @strongify(self);
            [self successGetCode:result];
        };
        
        _codeReuqest.errorBlock = ^(id error) {
            @strongify(self);
            [self errorGetCode];
        };
	}
	return _codeReuqest;
}

- (BZLoginCodeRequest *)loginCodeRequest {
	if(_loginCodeRequest == nil) {
		_loginCodeRequest = [[BZLoginCodeRequest alloc] init];
        @weakify(self);
        _loginCodeRequest.completeBlock = ^(id result) {
            @strongify(self);
            [self loginSuccess:result];
        };
        
        _loginCodeRequest.errorBlock = ^(id error) {
            @strongify(self);
            [self loginError];
        };
	}
	return _loginCodeRequest;
}

@end
