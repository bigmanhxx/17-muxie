//
//  BZLoginSuccessViewController.m
//  bz17
//
//  Created by bigMan on 16/6/30.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZLoginSuccessViewController.h"
#import "BZPopWindowHelper.h"
#import "BZBindNameRequest.h"

static CGFloat kHeadImageViewHeight = 70.f;

@interface BZLoginSuccessViewController()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UIImageView *userHeadImageView;
@property (nonatomic, strong) UIView      *userNameBackView;
@property (nonatomic, strong) UITextField *nameTextfiled;
@property (nonatomic, strong) UIButton    *loginButton;

@property (nonatomic, strong) UIImagePickerController *carmareViewController;
@property (nonatomic, strong) NSData *headImageData;

@property (nonatomic, strong) BZBindNameRequest *bindNameRequest;
@property (nonatomic, copy) NSString *key;

@end

@implementation BZLoginSuccessViewController

- (void)setParam:(NSDictionary *)param
{
    self.nameTextfiled.text = param[@"phoneNum"];
    self.key = param[@"key"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
}

- (void)popViewController
{
    [super popViewController];
    [BZSystemManager updatePersonKey:self.key];
}

- (void)initView
{
    self.title = @"登录成功";
    self.fd_interactivePopDisabled = YES;
    self.view.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
    [self.view addSubview:self.userHeadImageView];
    [self.userHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(60.f);
        make.centerX.equalTo(self.view);
        make.width.height.equalTo(@(kHeadImageViewHeight));
    }];
    {
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.text = @"上传头像";
        tipLabel.font = BZFont(14.f);
        tipLabel.textColor = [BZSystemManager colorWithHex:0x999999];
        [self.view addSubview:tipLabel];
        [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.userHeadImageView.mas_bottom).offset(7.f);
            make.centerX.equalTo(self.view);
        }];
        
    }
    [self.view addSubview:self.userNameBackView];
    [self.userNameBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@50.f);
        make.top.equalTo(self.userHeadImageView.mas_bottom).offset(48);
    }];
    {
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        nameLabel.text = @"昵称";
        nameLabel.font =BZFont(14.f);
        nameLabel.textColor = [BZSystemManager colorWithHex:0x666666];
        CGSize nameSize;
        nameSize = [nameLabel.text py_sizeForFont:nameLabel.font contentSize:CGSizeMake(1000, 1000)];
        [nameLabel setFrame:CGRectMake(16.f, 0, nameSize.width, 48.f)];
        [self.userNameBackView addSubview:nameLabel];
        
        [self.userNameBackView addSubview:self.nameTextfiled];
        [self.nameTextfiled setFrame:CGRectMake(nameLabel.right + 40, 0, SCREEN_WIDTH - nameLabel.right - 40, 48.f)];
    }
    [self.view addSubview:self.loginButton];
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(12.f);
        make.right.equalTo(self.view).offset(-12.f);
        make.height.equalTo(@48.f);
        make.top.equalTo(self.userNameBackView.mas_bottom).offset(39.f);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0)
{
    self.headImageData = [self dataOfImage:[image py_imageByScalingAndCroppingForSize:CGSizeMake(160, 160)]];
    self.userHeadImageView.image = image;
    [self.carmareViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - private method

- (void)openCarmare
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.carmareViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.navigationController presentViewController:self.carmareViewController animated:YES completion:^{
            
        }];
        return;
    }
    NSLog(@"相机未授权");
}


- (void)openPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.carmareViewController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.navigationController presentViewController:self.carmareViewController animated:YES completion:^{
            
        }];
        return;
    }
    NSLog(@"相册未授权");
}

- (void)openAcctionSheet
{
    @weakify(self);
    [[BZPopWindowHelper shareInstance] showPopWindow:BZPopWindowTypePhoto withBlock:^(id param) {
        @strongify(self);
        NSNumber *selectType = param;
        if (selectType.integerValue == 1) {
            [self openCarmare];
        } else {
            [self openPhoto];
        }
    }];
}

- (NSData *)dataOfImage:(UIImage *)image
{
    NSData *data;
    if (UIImagePNGRepresentation(image) == nil) {
        
        data = UIImageJPEGRepresentation(image, 0.3);
        
    } else {
        
        data = UIImagePNGRepresentation(image);
    }
    data = UIImageJPEGRepresentation(image, 0.05);
    self.userHeadImageView.image = image;
    return data;
}

- (void)bindAction
{
    if (!self.nameTextfiled.text.length) {
        [self showSimpleHUD:@"请填写昵称" afterDelay:1.0];
        return;
    }
    if (!self.headImageData) {
        [self showSimpleHUD:@"请上传头像" afterDelay:1.0];
        return;
    }
    [self showHUB];
    [self.bindNameRequest.requestParam addEntriesFromDictionary:@{
                                                                  @"headimg": self.headImageData ? self.headImageData : @"",
                                                                  @"nickname": self.nameTextfiled.text,
                                                                  @"key": self.key
                                                                  }];
    [self.bindNameRequest sendRequest];
}

- (void)bindSuccess:(id)result
{
    [BZSystemManager updatePersonKey:self.key];
    [BZSystemManager popToRootViewController];
}

- (void)bindFail
{
    [self showSimpleHUD:@"服务器错误" afterDelay:2.0];
}

#pragma mark - accessors method

- (UIImageView *)userHeadImageView {
	if(_userHeadImageView == nil) {
		_userHeadImageView = [[UIImageView alloc] init];
        _userHeadImageView.image = [UIImage imageNamed:@"default_user_head"];
        _userHeadImageView.userInteractionEnabled = YES;
        [_userHeadImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAcctionSheet)]];
        _userHeadImageView.layer.cornerRadius = kHeadImageViewHeight / 2.0;
        _userHeadImageView.layer.masksToBounds = YES;
	}
	return _userHeadImageView;
}

- (UIView *)userNameBackView {
	if(_userNameBackView == nil) {
		_userNameBackView = [[UIView alloc] init];
        _userNameBackView.backgroundColor = [UIColor whiteColor];
	}
	return _userNameBackView;
}

- (UITextField *)nameTextfiled {
	if(_nameTextfiled == nil) {
		_nameTextfiled = [[UITextField alloc] init];
        _nameTextfiled.clearButtonMode = UITextFieldViewModeAlways;
        _nameTextfiled.text = @"";
        _nameTextfiled.textColor = [BZSystemManager colorWithHex:0x666666];
	}
	return _nameTextfiled;
}

- (UIButton *)loginButton {
    if(_loginButton == nil) {
        _loginButton = [[UIButton alloc] init];
        [_loginButton setTitle:@"立即进入" forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:[BZSystemManager colorWithHex:0xff6a00]];
        [_loginButton addTarget:self action:@selector(bindAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginButton;
}
- (UIImagePickerController *)carmareViewController {
	if(_carmareViewController == nil) {
		_carmareViewController = [[UIImagePickerController alloc] init];
        _carmareViewController.delegate = self;
        _carmareViewController.allowsEditing = YES;
	}
	return _carmareViewController;
}

- (BZBindNameRequest *)bindNameRequest {
	if(_bindNameRequest == nil) {
		_bindNameRequest = [[BZBindNameRequest alloc] init];
        @weakify(self);
        _bindNameRequest.completeBlock = ^(id result) {
            @strongify(self);
            [self hiddenHUB];
            [self bindSuccess:result];
        };
        
        _bindNameRequest.errorBlock = ^(id error) {
            @strongify(self);
            [self bindFail];
        };
        
	}
	return _bindNameRequest;
}

@end
