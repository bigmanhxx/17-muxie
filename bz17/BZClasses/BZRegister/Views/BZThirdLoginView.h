//
//  BZThirdLoginView.h
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BZThirdUserInfo;

typedef void(^ShareResultBlock)(id userInfo);

@interface BZThirdLoginView : UIView

@property (nonatomic, copy) ShareResultBlock shareResultBlock;

@property (nonatomic, copy) void (^requestSendBlock)();

@end
