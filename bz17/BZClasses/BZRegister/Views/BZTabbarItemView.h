//
//  BZTabbarItemView.h
//  bz17
//
//  Created by yunhe.lin on 16/8/31.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BZTabbarSelectBlock)(NSInteger tag);

@interface BZTabbarItemView : UIView

- (instancetype)initWithSelectImage:(NSString *)selectImage normalImage:(NSString *)normalImage title:(NSString *)title;

@property (nonatomic, strong) UIColor *selectColor;
@property (nonatomic, strong) UIColor *normalColor;
@property (nonatomic, copy)   BZTabbarSelectBlock selectBlock;

- (void)setSelected:(BOOL)selected;

@end
