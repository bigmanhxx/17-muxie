//
//  BZTabbarView.m
//  bz17
//
//  Created by yunhe.lin on 16/8/31.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTabbarView.h"
#import "BZTabbarItemView.h"

@interface BZTabbarView()

@property (nonatomic, strong) NSMutableArray *tabbarItemViews;

@end

@implementation BZTabbarView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)setTabbarItems:(NSArray *)items
{
    [self removeAllSubviews];
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [BZSystemManager colorWithHex:0x00000 alpha:0.08];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.equalTo(@1);
    }];
    [self.tabbarItemViews removeAllObjects];
    [items enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BZTabbarItemView *tabbarItemView = [[BZTabbarItemView alloc] initWithSelectImage:obj[@"selectImage"] normalImage:obj[@"normalImage"] title:obj[@"title"]];
        @weakify(self);
        tabbarItemView.selectBlock = ^(NSInteger index){
            @strongify(self);
            [self.delegate bzSelectIndex:index];
        };
        [tabbarItemView setFrame:CGRectMake((SCREEN_WIDTH / items.count) * idx, 0, SCREEN_WIDTH / items.count, 49.f)];
        [self.tabbarItemViews addObject:tabbarItemView];
        tabbarItemView.tag = idx;
        [self addSubview:tabbarItemView];
    }];
    [self bringSubviewToFront:lineView];
}

- (void)setSelectIndex:(NSInteger)index
{
    [self.tabbarItemViews enumerateObjectsUsingBlock:^(BZTabbarItemView *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setSelected:index == idx];
    }];
}

- (void)refreshTabbarItem:(NSDictionary *)dict tabbarItemView:(BZTabbarItemView *)tabbarItemView
{
    
}

- (NSMutableArray *)tabbarItemViews {
	if(_tabbarItemViews == nil) {
		_tabbarItemViews = [[NSMutableArray alloc] init];
	}
	return _tabbarItemViews;
}

@end
