//
//  BZTabbarItemView.m
//  bz17
//
//  Created by yunhe.lin on 16/8/31.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTabbarItemView.h"

#define kTabbarSelectColor [BZSystemManager colorWithHex:0xffa033]

@interface BZTabbarItemView()

@property (nonatomic, strong) UIImageView *itemImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, copy)   NSString *selectImageStr;
@property (nonatomic, copy)   NSString *normalImagestr;
@property (nonatomic, strong) UIButton *clearBtn;
@end

@implementation BZTabbarItemView

- (instancetype)initWithSelectImage:(NSString *)selectImage normalImage:(NSString *)normalImage title:(NSString *)title
{
    if (self = [super init]) {
        [self initView];
        self.selectImageStr = selectImage;
        self.normalImagestr = normalImage;
        self.titleLabel.text = title;
        self.selectColor = kTabbarSelectColor;
        self.normalColor = [UIColor grayColor];
    }
    return self;
}

- (void)initView
{
    [self addSubview:self.itemImageView];
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(@(-20.f));
        make.width.height.lessThanOrEqualTo(@(24.5));
    }];
    
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self).offset(-2);
    }];
    
    [self addSubview:self.clearBtn];
    [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark - public method

- (void)setSelected:(BOOL)selected
{
    NSString *imageStr = selected ? self.selectImageStr : self.normalImagestr;
    UIColor *textColor = selected ? self.selectColor : self.normalColor;
    if ([imageStr hasPrefix:@"http"]) {
        [self.itemImageView sd_setImageWithURL:[NSURL URLWithString:imageStr]];
    } else {
        self.itemImageView.image = [UIImage imageNamed:imageStr];
    }
    self.titleLabel.textColor = textColor;
}

#pragma mark - private method 

- (void)clickClearBtn
{
    BLOCK_EXEC(self.selectBlock, self.tag);
}

#pragma mark - accessors method

- (UIImageView *)itemImageView {
	if(_itemImageView == nil) {
		_itemImageView = [[UIImageView alloc] init];
	}
	return _itemImageView;
}

- (UILabel *)titleLabel {
	if(_titleLabel == nil) {
		_titleLabel = [[UILabel alloc] init];
        _titleLabel.font = BZFont(12.f);
	}
	return _titleLabel;
}

- (UIButton *)clearBtn {
	if(_clearBtn == nil) {
		_clearBtn = [[UIButton alloc] init];
        [_clearBtn addTarget:self action:@selector(clickClearBtn) forControlEvents:UIControlEventTouchUpInside];
        [_clearBtn setBackgroundColor:[UIColor clearColor]];
	}
	return _clearBtn;
}

@end
