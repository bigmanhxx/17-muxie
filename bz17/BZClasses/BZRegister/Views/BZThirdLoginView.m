//
//  BZThirdLoginView.m
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZThirdLoginView.h"
#import "BZShareManager.h"
#import "BZThirdUserInfo.h"
#import "BZWechatBindRequest.h"
#import "BZQQBindRequest.h"
#import "BZSinaBindRequest.h"

@interface BZThirdLoginView()

@property (nonatomic, strong) UIImageView *wechatImageView;
@property (nonatomic, strong) UIImageView *qqImageView;
@property (nonatomic, strong) UIImageView *wbImageView;
@property (nonatomic, strong) BZThirdUserInfo *userInfo;
@property (nonatomic, strong) BZWechatBindRequest *wechatRequest;
@property (nonatomic, strong) BZQQBindRequest *qqBindRequest;
@property (nonatomic, strong) BZSinaBindRequest *sinaBindRequest;
@property (nonatomic, copy)   OnCompleteBlock completeBlock;
@property (nonatomic, copy)   OnErrorBlock    errorBlock;

@end

@implementation BZThirdLoginView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 85.f, 32.f)];
        label.text = @"快速登录";
        label.font = BZFont(14.f);
        label.textColor = [BZSystemManager colorWithHex:0xc7c7c7];
        [self addSubview:label];
        label.centerX = SCREEN_WIDTH / 2.0;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [BZSystemManager colorWithHex:0xc4c6cf];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(12.f);
            make.right.equalTo(self).offset(-12.f);
            make.height.equalTo(@1);
            make.centerY.equalTo(label);
        }];
        [self sendSubviewToBack:lineView];
    }
    [self addSubview:self.qqImageView];
    [self.qqImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).offset(50.f);
    }];
    {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"QQ";
        titleLabel.font = BZFont(14.f);
        titleLabel.textColor = [BZSystemManager colorWithHex:0x999999];
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.qqImageView);
            make.top.equalTo(self.qqImageView.mas_bottom).offset(10.f);
        }];
    }
    
    [self addSubview:self.wechatImageView];
    [self.wechatImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.qqImageView.mas_left).offset(-70.f * SCREEN_WIDTH / 375.f);
        make.top.equalTo(self.qqImageView);
    }];
    {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"微信";
        titleLabel.font = BZFont(14.f);
        titleLabel.textColor = [BZSystemManager colorWithHex:0x999999];
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.wechatImageView);
            make.top.equalTo(self.wechatImageView.mas_bottom).offset(10.f);
        }];
    }
//    [self addSubview:self.wbImageView];
//    [self.wbImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.qqImageView.mas_right).offset(70.f * SCREEN_WIDTH / 375.f);
//        make.top.equalTo(self.qqImageView);
//    }];
//    {
//        UILabel *titleLabel = [[UILabel alloc] init];
//        titleLabel.text = @"微博";
//        titleLabel.font = BZFont(14.f);
//        titleLabel.textColor = [BZSystemManager colorWithHex:0x999999];
//        [self addSubview:titleLabel];
//        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self.wbImageView);
//            make.top.equalTo(self.wbImageView.mas_bottom).offset(10.f);
//        }];
//    }
    @weakify(self);
    self.completeBlock = ^(id result) {
        @strongify(self);
        NSString *personKey = result[@"key"];
        if (personKey.length) {
            [BZSystemManager updatePersonKey:personKey];
            BLOCK_EXEC(self.shareResultBlock, result);
            return ;
        }
        self.userInfo.mid = result[@"mid"];
        BLOCK_EXEC(self.shareResultBlock, self.userInfo);
    };
    self.errorBlock = ^(id error) {
        @strongify(self);
        BLOCK_EXEC(self.shareResultBlock,error);
    };
    self.wechatRequest.completeBlock = self.completeBlock;
    self.qqBindRequest.completeBlock = self.completeBlock;
    self.sinaBindRequest.completeBlock = self.completeBlock;
    self.wechatRequest.errorBlock = self.errorBlock;
    self.wechatRequest.errorBlock = self.errorBlock;
    self.wechatRequest.errorBlock = self.errorBlock;
    
}

#pragma mark - private method
- (void)loginThird:(UIGestureRecognizer *)userInfo
{
    UIView *getstureView = userInfo.view;
    NSInteger viewTag = getstureView.tag - 1000;
    SSDKPlatformType platformType = SSDKPlatformTypeUnknown;
    switch (viewTag) {
        case 1:
            platformType = SSDKPlatformTypeWechat;
            break;
        case 2:
            platformType = SSDKPlatformTypeQQ;
            break;
        case 3:
            platformType = SSDKPlatformTypeSinaWeibo;
            break;
        default:
            break;
    }
    @weakify(self);
    [[BZShareManager shareInstance] thirdPathLogin:platformType resultSucces:^(BZThirdUserInfo *userInfo) {
        @strongify(self);
        NSLog(@"getSuccess ---  > %@", userInfo);
        self.userInfo = userInfo;
        [self getMidForUserInfo:platformType];
    } fail:^(id error) {
        @strongify(self);
        BLOCK_EXEC(self.shareResultBlock,error);
    }];
}

- (void)getMidForUserInfo:(SSDKPlatformType)platformType
{
    NSDictionary *param = @{
                            @"r": @"0",
                            @"c": @"0",
                            @"openid": self.userInfo.linkId,
                            @"headimgurl": self.userInfo.avatar,
                            @"nickname": self.userInfo.nickname
                            };
    BLOCK_EXEC(self.requestSendBlock);
    switch (platformType) {
        case SSDKPlatformTypeWechat:
            [self.wechatRequest.requestParam addEntriesFromDictionary:param];
            [self.wechatRequest sendRequest];
            break;
        case SSDKPlatformTypeQQ:
            [self.qqBindRequest.requestParam addEntriesFromDictionary:param];
            [self.qqBindRequest sendRequest];
            break;
        case SSDKPlatformTypeSinaWeibo:
            [self.sinaBindRequest.requestParam addEntriesFromDictionary:param];
            [self.sinaBindRequest sendRequest];
            break;
        default:
            break;
    }
}

#pragma mark - accessors method
- (UIImageView *)wechatImageView {
    if(_wechatImageView == nil) {
        _wechatImageView = [[UIImageView alloc] init];
        _wechatImageView.image = [UIImage imageNamed:@"login_wx_icon"];
        _wechatImageView.tag = 1001;
        _wechatImageView.userInteractionEnabled = YES;
        [_wechatImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginThird:)]];
    }
    return _wechatImageView;
}

- (UIImageView *)qqImageView {
    if(_qqImageView == nil) {
        _qqImageView = [[UIImageView alloc] init];
        _qqImageView.image = [UIImage imageNamed:@"login_qq_icon"];
        _qqImageView.tag = 1002;
        _qqImageView.userInteractionEnabled = YES;
        [_qqImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginThird:)]];
    }
    return _qqImageView;
}

- (UIImageView *)wbImageView {
    if(_wbImageView == nil) {
        _wbImageView = [[UIImageView alloc] init];
        _wbImageView.image = [UIImage imageNamed:@"login_wb_icon"];
        _wbImageView.tag = 1003;
        _wbImageView.userInteractionEnabled = YES;
        [_wbImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginThird:)]];
    }
    return _wbImageView;
}

- (BZWechatBindRequest *)wechatRequest {
	if(_wechatRequest == nil) {
		_wechatRequest = [[BZWechatBindRequest alloc] init];
	}
	return _wechatRequest;
}

- (BZQQBindRequest *)qqBindRequest {
	if(_qqBindRequest == nil) {
		_qqBindRequest = [[BZQQBindRequest alloc] init];
	}
	return _qqBindRequest;
}

- (BZSinaBindRequest *)sinaBindRequest {
	if(_sinaBindRequest == nil) {
		_sinaBindRequest = [[BZSinaBindRequest alloc] init];
	}
	return _sinaBindRequest;
}

@end
