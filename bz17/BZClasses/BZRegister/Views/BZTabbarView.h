//
//  BZTabbarView.h
//  bz17
//
//  Created by yunhe.lin on 16/8/31.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BZTabbarViewDelegate <NSObject>

@required
- (void)bzSelectIndex:(NSInteger)index;

@end

@interface BZTabbarView : UIView

@property (nonatomic, weak) id<BZTabbarViewDelegate> delegate;

- (void)setTabbarItems:(NSArray *)items;

- (void)setSelectIndex:(NSInteger)index;

@end
