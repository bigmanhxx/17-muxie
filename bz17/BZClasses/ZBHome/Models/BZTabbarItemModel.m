//
//  BZTabbarItemModel.m
//  bz17
//
//  Created by yunhe.lin on 16/7/19.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTabbarItemModel.h"

static NSString *kName = @"name";
static NSString *kUrl  = @"url";
static NSString *kSelectImage = @"selectImage";
static NSString *kNormalImage = @"normalImage";
static NSString *kSelectImageUrl = @"selectImageUrl";
static NSString *kNormalImageUrl = @"normalImageUrl";



@interface BZTabbarItemModel()<NSCoding>

@end

@implementation BZTabbarItemModel

- (void)encodeWithCoder:(NSCoder *)aCoder
{
     [aCoder encodeObject:self.name forKey:kName];
     [aCoder encodeObject:self.url forKey:kUrl];
     [aCoder encodeObject:self.normalImage forKey:kNormalImage];
     [aCoder encodeObject:self.selectImage forKey:kSelectImage];
     [aCoder encodeObject:self.imagechosed forKey:kSelectImageUrl];
     [aCoder encodeObject:self.imagenormal forKey:kNormalImageUrl];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.name = [aDecoder decodeObjectForKey:kName];
        self.url = [aDecoder decodeObjectForKey:kUrl];
        self.selectImage = [aDecoder decodeObjectForKey:kSelectImage];
        self.normalImage = [aDecoder decodeObjectForKey:kNormalImage];
        self.imagenormal = [aDecoder decodeObjectForKey:kNormalImageUrl];
        self.imagechosed = [aDecoder decodeObjectForKey:kSelectImageUrl];
    }
    return self;
}

@end
