//
//  BZTabbarItemModel.h
//  bz17
//
//  Created by yunhe.lin on 16/7/19.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BZTabbarItemModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *index;
@property (nonatomic, copy) NSString *imagenormal;
@property (nonatomic, copy) NSString *imagechosed;

@property (nonatomic, strong) UIImage *selectImage;
@property (nonatomic, strong) UIImage *normalImage;
@end
