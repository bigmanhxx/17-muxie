//
//  BZHomeBottomModel.h
//  bz17
//
//  Created by yunhe.lin on 16/7/19.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BZHomeBottomModel : NSObject<NSCoding>

@property (nonatomic, strong) NSArray *tabbar;

@property (nonatomic, copy)   NSString *domain;

@end
