//
//  BZHomeBottomModel.m
//  bz17
//
//  Created by yunhe.lin on 16/7/19.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZHomeBottomModel.h"
#import "MJExtension.h"

static NSString *kTabarKey = @"tabbar";

@implementation BZHomeBottomModel

+ (void)load
{
    [super load];
    [BZHomeBottomModel mj_setupObjectClassInArray:^NSDictionary *{
        return @{
                 @"tabbar": NSClassFromString(@"BZTabbarItemModel")
                 };
    }];
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.tabbar forKey:kTabarKey];
    [aCoder encodeObject:self.domain forKey:@"domain"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.tabbar = [aDecoder decodeObjectForKey:kTabarKey];
        self.domain = [aDecoder decodeObjectForKey:@"domain"];
    }
    return self;
}

//- (NSString *)domain
//{
//    if ([_domain py_hasContainStr:@"http"]) {
//        NSArray *strs = [_domain componentsSeparatedByString:@"http"];
//        if (strs.count > 1) {
//            return strs[1];
//        }
//    }
//    return _domain;
//}

@end
