//
//  BZMainHomeViewController.m
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZMainHomeViewController.h"
#import "BZBaseWebViewController.h"
#import "BZNetworkHelper.h"
#import "BZTestRequest.h"
#import "BZNavigationController.h"
#import "BZShareManager.h"
#import "BZLoginViewController.h"
#import "BZTokenRequest.h"
#import "BZHomeTabbarRequest.h"
#import "BZHomeBottomModel.h"
#import "BZTabbarItemModel.h"
#import "SVProgressHUD.h"
#import "BZTabbarView.h"
#import "BZTabarUrlManager.h"

typedef NS_ENUM(NSInteger, JSPatchType) {
    JSPatchTypeNone,
    JSPatchTypeOne,
    JSPatchTypeThree
};

#define kIconSize  CGSizeMake(24.5f, 24.5f)
#define kTabbarSelectColor [BZSystemManager colorWithHex:0xffa033]


@interface BZMainHomeViewController ()<UITabBarControllerDelegate, BZTabbarViewDelegate>

@property (nonatomic, strong) BZHomeTabbarRequest *homeTabarRequest;
@property (nonatomic, strong) BZTabbarView *tabbarView;

@end

@implementation BZMainHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [SVProgressHUD show];
    [self.homeTabarRequest sendRequest];
    [self.view addSubview:self.tabbarView];
    self.tabBar.hidden = YES;
    [BZTabarUrlManager setMainHomeViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)initView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.delegate = self;
    self.view.backgroundColor = [BZSystemManager colorWithHex:0xffffff];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tabBarController.tabBar.translucent = YES;
}

- (void)initNetTabbar:(BZHomeBottomModel *)homeTababrModel
{
    if (!homeTababrModel) {
        return;
    }
    [BZSystemManager setTabaarDefault:homeTababrModel];
    [self initTabarViewController:homeTababrModel];
}

- (void)initTabarViewController:(BZHomeBottomModel *)homeBottomModel
{
    __block NSMutableArray *viewControllers = @[].mutableCopy;
    __block NSMutableArray *items = @[].mutableCopy;
    [homeBottomModel.tabbar enumerateObjectsUsingBlock:^(BZTabbarItemModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (idx >= 2) {
            BZBaseWebViewController *wbViewController = [[BZBaseWebViewController alloc] initWithURLString:obj.url];
            wbViewController.title = obj.name;
            [viewControllers addObject:wbViewController];
            [items addObject:@{@"title": obj.name,
                               @"selectImage": obj.imagechosed,
                               @"normalImage": obj.imagenormal,
                               @"index": @(idx)}];
//        }
    }];
    [self setViewControllers:viewControllers animated:YES];
    [self.tabbarView setTabbarItems:items];
    self.selectedIndex = 0;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
    UIViewController *vc = self.childViewControllers[selectedIndex];
    [self py_addTitle:vc.title];
    [self.tabbarView setSelectIndex:selectedIndex];
}

#pragma mark - BZTabbarViewDelegate

- (void)bzSelectIndex:(NSInteger)index
{
    self.selectedIndex = index;
    BZBaseWebViewController *wbvc = self.childViewControllers[index];
    BLOCK_EXEC(wbvc.reloadBlock);
}

#pragma mark - UITabBarControllerDelegate

- (void)successGetHomeData:(BZHomeBottomModel *)result
{
    if (self.childViewControllers.count == 0) {
        [self initNetTabbar:result];
    }
}

- (void)errorGetHomeData:(id)error
{
    
    [self initNetTabbar:[BZSystemManager homeBottomModel]];
}

#pragma mark - public method

- (void)mainSelectIndex:(NSInteger)index
{
    self.selectedIndex = index;
}

#pragma mark - accessors method

- (BZHomeTabbarRequest *)homeTabarRequest
{
	if(_homeTabarRequest == nil) {
		_homeTabarRequest = [[BZHomeTabbarRequest alloc] init];
        @weakify(self);
        _homeTabarRequest.completeBlock = ^(BZHomeBottomModel *result) {
            @strongify(self);
            [SVProgressHUD dismiss];
            [self successGetHomeData:result];
        };
        
        _homeTabarRequest.errorBlock = ^(id error) {
            @strongify(self);
            [SVProgressHUD dismiss];
            [self errorGetHomeData:error];
        };
	}
	return _homeTabarRequest;
}

- (BZTabbarView *)tabbarView {
	if(_tabbarView == nil) {
		_tabbarView = [[BZTabbarView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 64.f - 49.f, SCREEN_WIDTH, 49.f)];
        _tabbarView.backgroundColor = [UIColor whiteColor];
        _tabbarView.delegate = self;
	}
	return _tabbarView;
}

@end
