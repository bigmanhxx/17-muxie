//
//  BZTabarUrlManager.h
//  bz17
//
//  Created by yunhe.lin on 16/9/9.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BZMainHomeViewController;

@interface BZTabarUrlManager : NSObject

+ (void)setMainHomeViewController:(BZMainHomeViewController *)mainVC;

+ (BOOL)filterUrlForRoot:(NSString *)url;

@end
