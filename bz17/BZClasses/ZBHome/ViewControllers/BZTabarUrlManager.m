//
//  BZTabarUrlManager.m
//  bz17
//
//  Created by yunhe.lin on 16/9/9.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTabarUrlManager.h"
#import "BZMainHomeViewController.h"
#import "BZBaseWebViewController.h"

static BZMainHomeViewController *mainHomeViewController = nil;

@implementation BZTabarUrlManager

+ (void)setMainHomeViewController:(BZMainHomeViewController *)mainVC
{
    mainHomeViewController =  mainVC;
}

+ (BOOL)filterUrlForRoot:(NSString *)url
{
    NSArray *vs = mainHomeViewController.viewControllers;
    __block NSInteger tabbarIndex = -1;
    __block BOOL hasUrl = false;
    [vs enumerateObjectsUsingBlock:^(BZBaseWebViewController *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.url isEqualToString:url]) {
            tabbarIndex = idx;
            hasUrl = YES;
            *stop = YES;
        }
    }];
    if (!hasUrl) {
        return NO;
    }
    [mainHomeViewController setSelectedIndex:tabbarIndex];
    [mainHomeViewController.navigationController popToRootViewControllerAnimated:YES];
    return YES;
}

@end
