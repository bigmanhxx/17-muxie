//
//  BZMainHomeViewController.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BZMainHomeViewController : UITabBarController

- (void)mainSelectIndex:(NSInteger)index;

@end
