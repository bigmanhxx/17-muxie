//
//  BZHomeTabbarRequest.m
//  bz17
//
//  Created by yunhe.lin on 16/7/14.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZHomeTabbarRequest.h"

@implementation BZHomeTabbarRequest

- (NSString *)methodName
{
    return @"/app/buttom";
}

- (NSString *)modelName
{
    return @"BZHomeBottomModel";
}

@end
