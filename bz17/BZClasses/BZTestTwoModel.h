//
//  BZTestTwoModel.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BZTestTwoModel : NSObject

@property (nonatomic, copy) NSString *testTwo;

@end
