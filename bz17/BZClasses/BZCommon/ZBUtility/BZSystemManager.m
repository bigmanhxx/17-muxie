//
//  BZSystemManager.m
//  bz17
//
//  Created by bigMan on 16/6/27.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZSystemManager.h"
#import "AppDelegate.h"
#import "BZPageGuideViewController.h"
#import "BZMainHomeViewController.h"
#import "BZNavigationController.h"
#import "BZVersionRequest.h"
#import "BZPopWindowHelper.h"
#import "BZHomeBottomModel.h"

static BZMainHomeViewController *homeViewController = nil;
static BZSystemManager *systemManager = nil;
static BZNavigationController *nowNavigation;

static NSString *kHomeBottomModelKey = @"kHomeBottomModelKey";

@implementation BZSystemManager

+ (instancetype)shareSystemManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        systemManager = [[BZSystemManager alloc] init];
    });
    return systemManager;
}

+ (void)stupGuideView:(BZMainHomeViewController *)rootViewController
{
    homeViewController = rootViewController;
    NSDate *openDate = [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_OPEN_DATE];
    if (openDate) {
        return;
    }
    BZPageGuideViewController *pageViewController = [[BZPageGuideViewController alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:FIRST_OPEN_DATE];
    [rootViewController presentViewController:pageViewController animated:NO completion:nil];
}

+ (void)updateNowNavigation:(id)navigation
{
    nowNavigation = navigation;
}

+ (void)pushViewController:(NSString *)viewController With:(NSDictionary *)param animated:(BOOL)animated
{
    BZBaseViewController *vc = [[NSClassFromString(viewController) alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    if (![vc respondsToSelector:@selector(setParam:)]) {
        return;
    }
    [vc performSelector:@selector(setParam:) withObject:param];
    [nowNavigation pushViewController:vc animated:animated];
}

+ (void)popToRootViewController
{
    [nowNavigation popToRootViewControllerAnimated:YES];
}

+ (UIColor *) colorWithHex:(NSInteger)hex {
    return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                           green:((float)((hex & 0xFF00) >> 8))/255.0
                            blue:((float)(hex & 0xFF))/255.0 alpha:1.0];
}

+ (UIColor *)colorWithHex:(NSInteger)hex alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                           green:((float)((hex & 0xFF00) >> 8))/255.0
                            blue:((float)(hex & 0xFF))/255.0 alpha:alpha];
}

+ (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (NSString *)version
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (void)updateVersion
{
    BZVersionRequest *versionRequest = [[BZVersionRequest alloc] init];
    versionRequest.completeBlock = ^(NSDictionary *result) {
        NSString *flagStr = result[@"result"];
        BOOL flag = [flagStr isEqualToString:@"true"];
        [self updateVersionFlag:flag];
        if (!flag) {
            return ;
        }
        //TODO: 更新view
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personKey"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginOutNotification" object:nil];
        [[BZPopWindowHelper shareInstance] showPopWindow:BZPopWindowTypeSystem withBlock:^(id param) {
            NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@",
                             @"com.muxie.huzhu"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }];
    };
    
    versionRequest.errorBlock = ^(id error) {
        
    };
    
    [versionRequest.requestParam addEntriesFromDictionary:@{
                                                            @"version": [self version],
                                                            @"channel": @"ios"
                                                            }];
    [versionRequest sendRequest];
}

+ (void)updateVersionFlag:(BOOL)flag
{
    [[NSUserDefaults standardUserDefaults] setObject:@(flag) forKey:@"updateFlag"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([self userLoginStatu] == BZLoginStatuLoginIn && flag) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personKey"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginOutNotification" object:nil];
    }
}

+ (BOOL)showUpdateFlag
{
    NSNumber *flag = [[NSUserDefaults standardUserDefaults] objectForKey:@"updateFlag"];
    return flag.boolValue;
}

+ (BOOL)updatePersonKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:key forKey:kPersonKey];
    BOOL flag = [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginChangeNotification" object:nil];
    return flag;
}

+ (NSString *)personKey
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPersonKey];
}

+ (void)setTabaarDefault:(BZHomeBottomModel *)homeBottomModel
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:homeBottomModel];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kHomeBottomModelKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BZHomeBottomModel *)homeBottomModel
{
    
    return [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:kHomeBottomModelKey]];
}

+ (BOOL)setDefaultObject:(id)object key:(NSString *)key
{
    if (object == nil || key == nil) {
        return NO;
    }
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    return [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)objectForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (BZLoginStatu)userLoginStatu
{
    NSString *personKey = [BZSystemManager personKey];
    if (personKey.length > 0) {
        return BZLoginStatuLoginIn;
    }
    return BZLoginStatuLoginOut;
}

@end
