//
//  BZSystemManager.h
//  bz17
//
//  Created by bigMan on 16/6/27.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BZLoginStatu) {
    BZLoginStatuLoginIn,
    BZLoginStatuLoginOut,
    BZLoginStatuLoginDisable
};

@class BZHomeBottomModel;

@interface BZSystemManager : NSObject


+ (void)stupGuideView:(UIViewController *)rootViewController;

+ (instancetype)shareSystemManager;
#pragma mark - 导航栏相关
/*!
 *  更新当前显示导航控制器
 *
 *  @param navigation
 */
+ (void)updateNowNavigation:(id)navigation;
+ (void)pushViewController:(NSString *)viewController With:(NSDictionary *)param animated:(BOOL)animated;
+ (void)popToRootViewController;
#pragma mark - 颜色设置
+ (UIColor *) colorWithHex:(NSInteger)hex;
+ (UIColor *)colorWithHex:(NSInteger)hex alpha:(CGFloat)alpha;

#pragma mark - 正则表达 验收手机
+ (BOOL)validateMobile:(NSString *)mobileNum;

#pragma mark - version
+ (NSString *)version;
+ (void)updateVersion;

+ (BOOL)updatePersonKey:(NSString *)key;

+ (NSString *)personKey;

+ (void)setTabaarDefault:(BZHomeBottomModel *)homeBottomModel;

+ (BZHomeBottomModel *)homeBottomModel;

+ (BOOL)setDefaultObject:(id)object key:(NSString *)key;

+ (id)objectForKey:(NSString *)key;

+ (BZLoginStatu)userLoginStatu;

+ (void)updateVersionFlag:(BOOL)flag;

+ (BOOL)showUpdateFlag;

@end
