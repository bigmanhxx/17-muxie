//
//  BZPushHelper.m
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZPushHelper.h"
#import "JPUSHService.h"

static NSString *jPushAppKey = @"19364eba42de3d475455aaa9";
#define IS_PRODUCTION !DEBUG

static BZPushHelper *pushHelper = nil;

@interface BZPushHelper()

@end

@implementation BZPushHelper

+ (id)instance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pushHelper = [[BZPushHelper alloc] init];
    });
    return pushHelper;
}

- (void)steupPushHeler:(NSDictionary *)launchingOption
{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    } else {
        //categories 必须为nil
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }
//    NSLog(@"%@", YES ? @"yes" : @"no");
    [JPUSHService setupWithOption:launchingOption appKey:jPushAppKey channel:@"" apsForProduction:YES advertisingIdentifier:@""];
}

@end
