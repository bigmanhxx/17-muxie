//
//  BZPushHelper.h
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BZPushHelper : NSObject

+ (id)instance;

- (void)steupPushHeler:(NSDictionary *)launchingOption;

@end
