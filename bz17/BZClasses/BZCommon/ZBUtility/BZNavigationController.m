//
//  BZNavigationController.m
//  bz17
//
//  Created by bigMan on 16/6/28.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZNavigationController.h"
#import "BZSystemManager.h"

@interface BZNavigationController()<UINavigationControllerDelegate>


@end

@implementation BZNavigationController

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    if (self = [super initWithRootViewController:rootViewController]) {
        self.delegate = self;
        self.navigationBarHidden = YES;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationBar.translucent = YES;
    }
    return self;
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
}

- (void)backAction
{
    [self popViewControllerAnimated:YES];
}
@end
