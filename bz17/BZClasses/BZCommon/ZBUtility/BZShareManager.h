//
//  BZShareManager.h
//  bz17
//
//  Created by bigMan on 16/6/29.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
@class BZThirdUserInfo;
typedef void(^successLoginBlock)(BZThirdUserInfo *userInfo);
typedef void(^failLoginBlock)(id error);

typedef void(^shareResultBlock)(SSDKResponseState responseState);

@interface BZShareManager : NSObject

+ (id)shareInstance;

/*!
 *  第三方登录
 *
 *  @param platformType 品台
 *  @param success      成功
 *  @param fail         失败  
 */
- (void)thirdPathLogin:(SSDKPlatformType)platformType resultSucces:(successLoginBlock)success fail:(failLoginBlock)fail;

/*!
 *  分享方法
 *
 *  @param param        分享参数
 *  @param platformType 分享类型
 *  @param resultBlock  回调
 */
- (void)shareParam:(NSDictionary *)param platform:(SSDKPlatformType)platformType resultShare:(shareResultBlock)resultBlock;

/*!
 *  安装分享
 */
- (void)shareSteup;

@end
