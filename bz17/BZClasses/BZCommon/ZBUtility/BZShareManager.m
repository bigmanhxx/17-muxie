//
//  BZShareManager.m
//  bz17
//
//  Created by bigMan on 16/6/29.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZShareManager.h"

//【QQ】
//app_id=101313487
//app_key=7948add7139f9c53d1645f376ae710b8
//
//【微博】
//key=258858545
//secret=58bd5661131dfc3240f0025b4c8f3ea8
//
//【微信】
//app_id=wx9da2f5c2cb792a30
//appsecret=1a5172ce042127520b489fc1638d7d98

#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import "WeiboSDK.h"
#import "BZThirdUserInfo.h"
#import "MBProgressHUD.h"
//#import "Diplomat.h"
//#import <Diplomat/WechatProxy.h>
//#import <Diplomat/QQProxy.h>


static NSString *shareSDKKey = @"11e4b450c9fe2";

static NSString *weixin_app_id = @"wx9da2f5c2cb792a30";//@"wx4868b35061f87885";//;
static NSString *weixin_app_secret = @"1a5172ce042127520b489fc1638d7d98";//@"64020361b8ec4c99936c0e3999a9f249";//

static NSString *qq_app_id = @"100371282";//@"101313487";
static NSString *qq_app_secret = @"aed9b0303e3ed1e27bae87c33761161d";//@"7948add7139f9c53d1645f376ae710b8";

static NSString *weibo_app_id = @"258858545"; //@"568898243";//
static NSString *weibo_app_secret = @"58bd5661131dfc3240f0025b4c8f3ea8";//@"38a4f8204cc784f81f9f0daaf31e02e3";//

static BZShareManager *shareManager = nil;

@interface BZShareManager()<WXApiDelegate>

@end

@implementation BZShareManager

+ (id)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[BZShareManager alloc] init];
    });
    return shareManager;
}

- (void)shareSteup
{
//    [[Diplomat sharedInstance] registerWithConfigurations:@{kDiplomatTypeWechat: @{kDiplomatAppIdKey: weixin_app_id,
//                                                                                   kDiplomatAppSecretKey: weixin_app_secret},
//                                                            kDiplomatTypeQQ: @{kDiplomatAppIdKey: qq_app_id},
//                                                            }];

    
    //  第三方登录
    [ShareSDK registerApp:shareSDKKey
          activePlatforms:@[@(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ),
                            @(SSDKPlatformTypeSinaWeibo)]
                 onImport:^(SSDKPlatformType platformType) {
                     switch (platformType) {
                         case SSDKPlatformTypeWechat:
                             [ShareSDKConnector connectWeChat:[WXApi class] delegate:self];
                             break;
                         case SSDKPlatformTypeQQ:
                             [ShareSDKConnector connectQQ:[QQApiInterface class]
                                        tencentOAuthClass:[TencentOAuth class]];
                             break;
                             
                         case SSDKPlatformTypeSinaWeibo:
                             [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                             break;
                         default:
                             break;
                     }
                 } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
                     switch (platformType) {
                         case SSDKPlatformTypeWechat:
                             [appInfo SSDKSetupWeChatByAppId:weixin_app_id appSecret:weixin_app_secret];
                             break;
                         case SSDKPlatformTypeQQ:
                             [appInfo SSDKSetupQQByAppId:qq_app_id
                                                  appKey:qq_app_secret
                                                authType:SSDKAuthTypeBoth];
                             break;
                             
                         case SSDKPlatformTypeSinaWeibo:
                             [appInfo SSDKSetupSinaWeiboByAppKey:weibo_app_id
                                                       appSecret:weibo_app_secret
                                                     redirectUri:@"http://www.sharesdk.cn"
                                                        authType:SSDKAuthTypeBoth];
                             break;
                         default:
                             break;
                     }
                 }];
    [SSEThirdPartyLoginHelper setUserClass:[BZThirdUserInfo class]];
}

- (void)thirdPathLogin:(SSDKPlatformType)platformType resultSucces:(successLoginBlock)success fail:(failLoginBlock)fail
{
    [SSEThirdPartyLoginHelper loginByPlatform:platformType onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
        associateHandler(user.uid, user, user);
    } onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
        if (SSDKResponseStateSuccess == state) {
            BZThirdUserInfo *bzUser = (BZThirdUserInfo *)user;
            success(bzUser);
        } else {
            fail(nil);
        }
    }];
}

- (void)shareParam:(NSDictionary *)param platform:(SSDKPlatformType)platformType resultShare:(shareResultBlock)resultBlock
{
    
    NSMutableDictionary *shareParam = [NSMutableDictionary dictionaryWithCapacity:1];
    [shareParam SSDKSetupShareParamsByText:param[@"text"] images:param[@"image"] ? param[@"image"] : @[[UIImage imageNamed:@"share_wx_icon"]] url:param[@"url"] title:param[@"title"] type:SSDKContentTypeAuto];
    [ShareSDK share:platformType parameters:shareParam onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        resultBlock(state);
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
                                                                    message:nil
                                                                   delegate:nil
                                                          cancelButtonTitle:@"确定"
                                                          otherButtonTitles:nil];
                [alertView show];
                break;
            }
            case SSDKResponseStateFail:
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                    message:[NSString stringWithFormat:@"%@", error]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"确定"
                                                          otherButtonTitles:nil];
                [alertView show];
                break;
            }
            default:
                break;
        }
    }];
    
}


- (void)onResp:(BaseResp *)resp
{
    
}

@end
