//
//  BZAdViewController.m
//  bz17
//
//  Created by bigMan on 16/6/27.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZAdViewController.h"

static const CGFloat buttonWidth = 60.f;
static const CGFloat buttonHeight = 25.f;

@interface BZAdViewController()

@property (nonatomic, strong) UIImageView *adImageView;
@property (nonatomic, strong) UIButton    *timeButton;
@property (nonatomic, strong) NSTimer     *timer;
@property (nonatomic, assign) NSInteger   times;

@end

@implementation BZAdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.times = 5;
    [self.view addSubview:self.adImageView];
    [self.view addSubview:self.timeButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.timer invalidate];
}

- (void)setAdData:(NSDictionary *)adDict
{
    self.adImageView.image = [UIImage imageNamed:@"welcome_guide_1"];
}

- (void)countDown
{
    [self.timeButton setTitle:[NSString stringWithFormat:@"%zdS",self.times] forState:UIControlStateNormal];
    self.times--;
    if (!self.times) {
        // 倒计时结束
        [self.timer invalidate];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)closeAdViewController
{
    [self.timer invalidate];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - accessors method

- (UIImageView *)adImageView {
	if(_adImageView == nil) {
		_adImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _adImageView.backgroundColor = [UIColor redColor];
	}
	return _adImageView;
}

- (UIButton *)timeButton {
	if(_timeButton == nil) {
		_timeButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - buttonWidth - 20, 20, buttonWidth, buttonHeight)];
        _timeButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _timeButton.layer.cornerRadius = 6.0;
        [_timeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_timeButton addTarget:self action:@selector(closeAdViewController) forControlEvents:UIControlEventTouchUpInside];
        [_timeButton setTitle:[NSString stringWithFormat:@"%zdS",self.times] forState:UIControlStateNormal];
	}
	return _timeButton;
}

- (NSTimer *)timer {
	if(_timer == nil) {
		_timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(countDown) userInfo:nil repeats:YES];
	}
	return _timer;
}

@end
