//
//  BZPageGuideViewController.m
//  bz17
//
//  Created by bigMan on 16/6/27.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZPageGuideViewController.h"
#import "BZAdViewController.h"
#import "AppDelegate.h"

@interface BZPageGuideViewController()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *pageScrollView;

@property (nonatomic, strong) UIPageControl *pageControl;

@end

@implementation BZPageGuideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.pageScrollView];
    [self.view addSubview:self.pageControl];
    for (NSInteger i = 0; i < 4; i++) {
        [self.pageScrollView addSubview:[self guideImageView:i]];
    }
}

- (UIImageView *)guideImageView:(NSInteger)index
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * index, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"welcome_guide_%zd", index + 1]];
    imageView.backgroundColor = [UIColor yellowColor];
    if (index == 3) {
        imageView.userInteractionEnabled = YES;
        [imageView addSubview:({
            UIButton *clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            clickBtn.frame = CGRectMake((SCREEN_WIDTH - 270)/2, SCREEN_HEIGHT - 100, 270, 50);
            [clickBtn setImage:[UIImage imageNamed:@"guideBtn"] forState:UIControlStateNormal];
            [clickBtn addTarget:self action:@selector(clickLastImageView:) forControlEvents:UIControlEventTouchUpInside];
            clickBtn;
        })];
    }
    return imageView;
}

- (void)clickLastImageView:(UIButton *)btn
{
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page= _pageScrollView.contentOffset.x/self.view.viewWidth;
    _pageControl.currentPage=page;
    
}

#pragma mark - accessors method

- (UIScrollView *)pageScrollView {
    if(_pageScrollView == nil) {
        _pageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _pageScrollView.pagingEnabled = YES;
        _pageScrollView.showsHorizontalScrollIndicator = NO;
        _pageScrollView.showsVerticalScrollIndicator = NO;
        _pageScrollView.backgroundColor = [UIColor whiteColor];
        _pageScrollView.delegate = self;
        _pageScrollView.bounces = NO;
        _pageScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 4.0, SCREEN_HEIGHT);
    }
    return _pageScrollView;
}

- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame: CGRectMake(0, self.view.viewHeight - 40, self.view.viewWidth, 30)];
        _pageControl.numberOfPages = 4;
        _pageControl.currentPageIndicatorTintColor = [UIColor py_colorWithHex:0xFF6A00];
        _pageControl.pageIndicatorTintColor = [UIColor py_colorWithHex:0xDCDEE3];
        _pageControl.currentPage = 0;
    }
    return _pageControl;
}

@end
