//
//  BZPopWindowHelper.m
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZPopWindowHelper.h"
#import "BZShareView.h"
#import "BZSelectPhotoMethodView.h"
#import "BZUpdateView.h"

static BZPopWindowHelper *popWindowHelper = nil;

@interface BZPopWindowHelper()

@property (nonatomic, strong) UIWindow *hightWindow;
@property (nonatomic, strong) UIView   *backView;
@property (nonatomic, strong) UIView   *cureentView;
@property (nonatomic, strong) BZShareView *shareView;
@property (nonatomic, strong) BZUpdateView *updateView;
@property (nonatomic, strong) BZSelectPhotoMethodView *selectPhotoView;
@property (nonatomic, copy)   finishBlock completeBlock;


@end

@implementation BZPopWindowHelper

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        popWindowHelper = [[BZPopWindowHelper alloc] init];
    });
    return popWindowHelper;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self initHelper];
    }
    return self;
}

- (void)initHelper
{
    [self.hightWindow addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.hightWindow);
        make.height.equalTo(@(SCREEN_HEIGHT));
    }];
    self.hightWindow.backgroundColor = [UIColor clearColor];
}

#pragma mark - public method 

- (void)showPopWindow
{
    [self.hightWindow makeKeyWindow];
    self.hightWindow.hidden = NO;
}

- (void)hidePopWindow
{
    [self.hightWindow resignFirstResponder];
    self.hightWindow.hidden = YES;
    [self.cureentView removeFromSuperview];
}

- (void)showPopWindow:(BZPopWindowType)popWindowType withBlock:(finishBlock)block
{
    [self showPopWindow];
    self.completeBlock = block;
    switch (popWindowType) {
        case BZPopWindowTypeShare:
        {
            [self shareAction:block];
            break;
        }
        case BZPopWindowTypePhoto:
        {
            [self photoAction:block];
            [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(SCREEN_HEIGHT - self.cureentView.viewHeight));
            }];
            break;
        }
        case BZPopWindowTypeSystem:
        {
            [self systemBlock:block];
            [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(SCREEN_HEIGHT));
            }];
            break;
        }
        default:
            break;
    }
}

#pragma mark - private method 

- (void)shareAction:(finishBlock)blcok
{
    self.cureentView = self.shareView;
    [self.hightWindow addSubview:self.shareView];
}

- (void)shareWithType:(NSInteger)type
{
    [self hidePopWindow];
    BLOCK_EXEC(self.completeBlock,@(type));
}

- (void)photoAction:(finishBlock)block
{
    self.cureentView = self.selectPhotoView;
    [self.hightWindow addSubview:self.selectPhotoView];
}

- (void)photoWithType:(BZSelectPhotoMethod)method
{
    [self hidePopWindow];
    if (method == BZSelectPhotoMethodCancel) {
        return;
    }
    BLOCK_EXEC(self.completeBlock,@(method));
}

- (void)systemBlock:(finishBlock)block
{
    self.backView.userInteractionEnabled = NO; // 不可以消失
    //TODO: 强制更新view
    self.cureentView = self.updateView;
    [self.hightWindow addSubview:self.updateView];
}

#pragma mark - accessors method

- (UIWindow *)hightWindow {
	if(_hightWindow == nil) {
		_hightWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	}
	return _hightWindow;
}

- (UIView *)backView {
	if(_backView == nil) {
		_backView = [[UIView alloc] init];
        _backView.backgroundColor = [BZSystemManager colorWithHex:0x333333 alpha:0.5];
        [_backView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopWindow)]];
        _backView.userInteractionEnabled = YES;
	}
	return _backView;
}

- (BZShareView *)shareView {
	if(_shareView == nil) {
		_shareView = [[BZShareView alloc] init];
        @weakify(self);
        _shareView.cancelBlock = ^() {
            @strongify(self);
            [self hidePopWindow];
        };
        
        _shareView.shareBlock = ^(NSInteger shareType) {
            @strongify(self);
            [self shareWithType:shareType];
        };
	}
	return _shareView;
}

- (BZSelectPhotoMethodView *)selectPhotoView {
	if(_selectPhotoView == nil) {
		_selectPhotoView = [[BZSelectPhotoMethodView alloc] init];
        @weakify(self);
        _selectPhotoView.selectPhotoMethodBlock = ^(BZSelectPhotoMethod method) {
            @strongify(self);
            [self photoWithType:method];
        };
	}
	return _selectPhotoView;
}

- (BZUpdateView *)updateView
{
	if(_updateView == nil) {
		_updateView = [[BZUpdateView alloc] init];
        @weakify(self);
        _updateView.sureBlock = ^{
            @strongify(self);
            BLOCK_EXEC(self.completeBlock, nil);
            [self hidePopWindow];
        };
	}
	return _updateView;
}

@end
