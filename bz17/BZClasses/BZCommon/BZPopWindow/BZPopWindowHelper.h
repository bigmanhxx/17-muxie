//
//  BZPopWindowHelper.h
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^finishBlock)(id param);

typedef NS_ENUM(NSInteger, BZPopWindowType) {
    BZPopWindowTypeShare = 1,       // 分享类型
    BZPopWindowTypePhoto,           // 照片类型
    BZPopWindowTypeSystem,          // 强制升级
};

@interface BZPopWindowHelper : NSObject

+ (instancetype)shareInstance;

- (void)showPopWindow:(BZPopWindowType)popWindowType withBlock:(finishBlock)block;

- (void)showPopWindow;

- (void)hidePopWindow;

@end
