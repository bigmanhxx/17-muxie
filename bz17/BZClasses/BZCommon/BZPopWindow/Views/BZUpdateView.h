//
//  BZUpdateView.h
//  bz17
//
//  Created by yunhe.lin on 16/9/18.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BZUpdateView : UIView

@property (nonatomic, copy) void(^sureBlock)();

@end
