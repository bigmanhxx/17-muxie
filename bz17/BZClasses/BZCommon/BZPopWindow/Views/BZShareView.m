//
//  BZShareView.m
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZShareView.h"
#import "BZShareViewCell.h"
#import <ShareSDK/ShareSDK.h>

static CGFloat viewHeight = (250.f + 98.f) / 2.0;
static NSString *kShareCollectionViewCellID = @"shareCollectionViewCellID";

@interface BZShareView()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *shareCollectionView;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSArray *shareTypes;

@end

@implementation BZShareView

- (id)init
{
    return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    self.dataArray = @[@{
                           @"icon": @"share_wxf_icon",
                           @"title": @"微信"
                           },
                       @{
                           @"icon": @"share_wx_icon",
                           @"title": @"朋友圈"
                           },
                       @{
                           @"icon": @"share_qq_icon",
                           @"title": @"QQ好友"
                           },
                       @{
                           @"icon": @"share_icon_wb",
                           @"title": @"微博"
                           }];
    self.shareTypes = @[@(SSDKPlatformSubTypeWechatSession), @(SSDKPlatformSubTypeWechatTimeline), @(SSDKPlatformTypeQQ), @(SSDKPlatformTypeSinaWeibo)];
    [self setFrame:CGRectMake(0, SCREEN_HEIGHT - viewHeight, SCREEN_WIDTH, viewHeight)];
    self.userInteractionEnabled = YES;
    [self addSubview:self.shareCollectionView];
    {
        UIButton *cancelButton = [[UIButton alloc] init];
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancelButton setBackgroundColor:[UIColor whiteColor]];
        [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cancelButton.titleLabel setFont:BZFont(18.f)];
        [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelButton];
        [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.equalTo(@49.f);
        }];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BZShareViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kShareCollectionViewCellID forIndexPath:indexPath];
    [cell refreshCell:self.dataArray[indexPath.item]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BLOCK_EXEC(self.shareBlock, [self.shareTypes[indexPath.item] integerValue]);
}

#pragma mark - private method 

- (void)cancelAction
{
    BLOCK_EXEC(self.cancelBlock);
}

#pragma mark - accessors method

- (UICollectionView *)shareCollectionView {
	if(_shareCollectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 9.f;
        layout.minimumInteritemSpacing = 9.f;
        layout.itemSize = CGSizeMake(iconWidth, iconHeight);
        layout.sectionInset = UIEdgeInsetsMake(Padding, Padding, 20.f, Padding);
		_shareCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, viewHeight - 49.f) collectionViewLayout:layout];
        _shareCollectionView.delegate = self;
        _shareCollectionView.dataSource = self;
        _shareCollectionView.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
        _shareCollectionView.showsVerticalScrollIndicator = NO;
        _shareCollectionView.showsHorizontalScrollIndicator = NO;
        [_shareCollectionView registerClass:[BZShareViewCell class] forCellWithReuseIdentifier:kShareCollectionViewCellID];
	}
	return _shareCollectionView;
}

@end
