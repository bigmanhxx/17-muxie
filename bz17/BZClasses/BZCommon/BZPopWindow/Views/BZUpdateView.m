//
//  BZUpdateView.m
//  bz17
//
//  Created by yunhe.lin on 16/9/18.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZUpdateView.h"

@interface BZUpdateView()

@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIButton *sureButton;

@end

@implementation BZUpdateView


- (instancetype)init
{
    if (self = [super init]) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    [self setFrame:CGRectMake(SCREEN_WIDTH / 8.f, (SCREEN_HEIGHT - SCREEN_WIDTH / 4.0 * 3.0) / 2.0, SCREEN_WIDTH / 4.0 * 3.0, SCREEN_WIDTH / 2.0)];
    [self setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(SCREEN_WIDTH / 2.0 - 44));
    }];
    
    [self addSubview:self.sureButton];
    [self.sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@44);
    }];
}

#pragma mark - event handler 

- (void)sureAction
{
    BLOCK_EXEC(self.sureBlock);
}

#pragma mark - accessors method

- (UILabel *)descLabel
{
	if(_descLabel == nil) {
		_descLabel = [[UILabel alloc] init];
        _descLabel.textColor = [UIColor blackColor];
        _descLabel.font = BZFont(15.f);
        _descLabel.textAlignment = NSTextAlignmentCenter;
        _descLabel.text = @"有新版本更新，是否获取最新版本";
	}
	return _descLabel;
}

- (UIButton *)sureButton
{
	if(_sureButton == nil) {
		_sureButton = [[UIButton alloc] init];
        [_sureButton setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_sureButton addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
	}
	return _sureButton;
}

@end
