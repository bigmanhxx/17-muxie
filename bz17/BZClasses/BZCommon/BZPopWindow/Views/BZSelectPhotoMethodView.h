//
//  BZSelectPhotoMethodView.h
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BZSelectPhotoMethod) {
    BZSelectPhotoMethodCarame = 1,
    BZSelectPhotoMethodPhoto,
    BZSelectPhotoMethodCancel
};

typedef void(^SelectPhotoMethodBlock)(BZSelectPhotoMethod method);

@interface BZSelectPhotoMethodView : UIView

@property (nonatomic, copy) SelectPhotoMethodBlock selectPhotoMethodBlock;

@end
