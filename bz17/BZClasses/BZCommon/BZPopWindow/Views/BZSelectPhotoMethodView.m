//
//  BZSelectPhotoMethodView.m
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZSelectPhotoMethodView.h"

static CGFloat SelectViewHeight = (98.f + 19.f + 197.f) / 2.0;

@interface BZSelectPhotoMethodView()

@property (nonatomic, strong) UIButton *carameButton;
@property (nonatomic, strong) UIButton *photoButton;
@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation BZSelectPhotoMethodView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    self.backgroundColor = [BZSystemManager colorWithHex:0xf0f0f0];
    [self setFrame:CGRectMake(0, SCREEN_HEIGHT - SelectViewHeight, SCREEN_WIDTH, SelectViewHeight)];
    [self addSubview:self.carameButton];
    [self.carameButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.equalTo(@(197.f / 4.f));
    }];
    [self addSubview:self.photoButton];
    [self.photoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.carameButton.mas_bottom);
        make.height.equalTo(@(197.f / 4.f));
    }];
    [self addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(49.f));
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [BZSystemManager colorWithHex:0xdddddd];
    [self.carameButton addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.carameButton).offset(17.5f);
        make.right.bottom.equalTo(self.carameButton);
        make.height.equalTo(@1);
    }];
}

#pragma mark - private method 

- (void)selectMethod:(UIButton *)selectButton
{
    BZSelectPhotoMethod method = selectButton.tag;
    BLOCK_EXEC(self.selectPhotoMethodBlock,method);
}

#pragma mark accessors method

- (UIButton *)carameButton {
	if(_carameButton == nil) {
		_carameButton = [[UIButton alloc] init];
        _carameButton.tag = BZSelectPhotoMethodCarame;
        [_carameButton setTitle:@"拍照" forState:UIControlStateNormal];
        [_carameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_carameButton setBackgroundColor:[UIColor whiteColor]];
        [_carameButton addTarget:self action:@selector(selectMethod:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _carameButton;
}

- (UIButton *)photoButton {
	if(_photoButton == nil) {
		_photoButton = [[UIButton alloc] init];
        _photoButton.tag = BZSelectPhotoMethodPhoto;
        [_photoButton setTitle:@"从手机相册选择" forState:UIControlStateNormal];
        [_photoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_photoButton setBackgroundColor:[UIColor whiteColor]];
        [_photoButton addTarget:self action:@selector(selectMethod:) forControlEvents:UIControlEventTouchUpInside];
    }
	return _photoButton;
}

- (UIButton *)cancelButton {
	if(_cancelButton == nil) {
		_cancelButton = [[UIButton alloc] init];
        _cancelButton.tag = BZSelectPhotoMethodCancel;
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor whiteColor]];
        [_cancelButton addTarget:self action:@selector(selectMethod:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _cancelButton;
}

@end
