//
//  BZShareViewCell.m
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZShareViewCell.h"

@interface BZShareViewCell()

@property (nonatomic, strong) UIView      *iconBackView;
@property (nonatomic, strong) UIImageView *iconImage;
@property (nonatomic, strong) UILabel     *nameLabel;

@end

@implementation BZShareViewCell

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.iconImage];
    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.nameLabel.mas_top).offset(-15.f);
    }];
}

#pragma mark - public method 

- (void)refreshCell:(NSDictionary *)dict
{
    self.nameLabel.text = dict[@"title"];
    self.iconImage.image = [UIImage imageNamed:dict[@"icon"]];
}

#pragma mark - accessors method

- (UIImageView *)iconImage {
	if(_iconImage == nil) {
		_iconImage = [[UIImageView alloc] init];
	}
	return _iconImage;
}

- (UILabel *)nameLabel {
	if(_nameLabel == nil) {
		_nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [BZSystemManager colorWithHex:0x666666];
        _nameLabel.font = BZFont(12.f);
        _nameLabel.text = @"朋友圈";
	}
	return _nameLabel;
}

- (UIView *)iconBackView {
	if(_iconBackView == nil) {
		_iconBackView = [[UIView alloc] init];
        _iconBackView.layer.cornerRadius = 13.f;
        _iconBackView.backgroundColor = [UIColor whiteColor];
	}
	return _iconBackView;
}

@end
