//
//  BZShareViewCell.h
//  bz17
//
//  Created by bigMan on 16/7/3.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>
#define Padding (SCREEN_WIDTH / 375.f * 27.f)
#define iconWidth (SCREEN_WIDTH - Padding * 5.f) / 4.f
#define iconHeight ((250.f - 42.f - 47.f) / 2.0)

@interface BZShareViewCell : UICollectionViewCell

- (void)refreshCell:(NSDictionary *)dict;

@end
