//
//  BZShareView.h
//  bz17
//
//  Created by bigMan on 16/7/1.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CancelBlock)(void);
typedef void(^ShareBlock) (NSInteger shareType);

@interface BZShareView : UIView

@property (nonatomic, copy) CancelBlock cancelBlock;

@property (nonatomic, copy) ShareBlock   shareBlock;

@end
