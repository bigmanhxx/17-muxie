//
//  BZThirdUserInfo.h
//  bz17
//
//  Created by bigMan on 16/6/29.
//  Copyright © 2016年 17. All rights reserved.
//

#import <ShareSDKExtension/SSEBaseUser.h>

@interface BZThirdUserInfo : SSEBaseUser

/**
 *  头像
 */
@property (nonatomic, copy) NSString *avatar;

/**
 *  昵称
 */
@property (nonatomic, copy) NSString *nickname;

/**
 *  描述
 */
@property (nonatomic, copy) NSString *aboutMe;

/*!
 *  服务器 -17 标识位mid
 */
@property (nonatomic, copy) NSString *mid;

@property (nonatomic, assign) NSInteger platformType;

@end
