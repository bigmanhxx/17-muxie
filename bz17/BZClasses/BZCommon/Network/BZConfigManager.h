//
//  BZConfigManager.h
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^TokenBlock)(NSString *token);

@interface BZConfigManager : NSObject

+ (void)requestToken:(TokenBlock)tokenBlock;

@end
