//
//  BZConfigManager.m
//  bz17
//
//  Created by bigMan on 16/7/4.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZConfigManager.h"
#import "BZTokenRequest.h"

#define REQUEST_TOKEN @"request_token"

#define REQUEST_DATE_TOKEN @"request_date_token"

@implementation BZConfigManager

+ (void)requestToken:(TokenBlock)tokenBlock
{
    NSUserDefaults *tokenDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [tokenDefault objectForKey:REQUEST_TOKEN];
    NSDate *lastToken = [tokenDefault objectForKey:REQUEST_DATE_TOKEN];
    if (!lastToken) {
        [self token:tokenBlock];
    }
    NSDate *nowDate = [NSDate date];
    NSInteger seconds = ([nowDate timeIntervalSince1970] - [lastToken timeIntervalSince1970]) - 7000;
    if (seconds > 0) {
        [self token:tokenBlock];
    } else {
        tokenBlock(token);
    }
}

+ (void)token:(TokenBlock)tokenBlock
{
    BZTokenRequest *tokenRequest = [[BZTokenRequest alloc] init];
    tokenRequest.completeBlock = ^(id result) {
        NSDictionary *dict = result;
        NSString *token = dict[@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:REQUEST_TOKEN];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:REQUEST_DATE_TOKEN];
        [[NSUserDefaults standardUserDefaults] synchronize];
        tokenBlock(dict[@"token"]);
        
    };
    tokenRequest.errorBlock = ^(id error) {
        tokenBlock(@"");
    };
    [tokenRequest sendRequest];
}

@end
