//
//  BZBaseRequest.m
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseRequest.h"
#import "BZNetworkHelper.h"
#import "BZConfigManager.h"


#define VERSION_APP @"0.1"
typedef void(^RequestBlock)(void);

@implementation BZBaseRequest

- (instancetype)init
{
    if (self = [super init]) {
        self.requestParam = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return self;
}

- (BZHttpType)httpType
{
    return BZHttpTypePOST;
}

- (NSString *)url
{
    return [NSString stringWithFormat:@"%@%@",baseUrl,[self methodName]];
}

- (NSString *)modelName
{
    return @"";
}

- (void)sendRequest
{
    RequestBlock requestBlock = ^(void) {
        NSString *url = [self url];
        if (!url.length) {
            return;
        }
        [self.requestParam addEntriesFromDictionary:@{
                                                      @"version": VERSION_APP
                                                      }];
        switch ([self httpType]) {
            case BZHttpTypePOST:
            {
                [[BZNetworkHelper shareNetworkHelper] postWithUrl:url refreshCache:true params:self.requestParam success:self.completeBlock fail:self.errorBlock modelName:[self modelName]];
                break;
            }
            case BZHttpTypeGET:
            {
                [[BZNetworkHelper shareNetworkHelper] getWithUrl:url refreshCache:true params:self.requestParam success:self.completeBlock fail:self.errorBlock modelName:[self modelName]];
                break;
            }
            default:
                break;
        }
    };
    if ([[self methodName] isEqualToString:@"/app/token"]) {
        requestBlock();
    } else {
       [BZConfigManager requestToken:^(NSString *token) {
           if (!token.length) {
               self.errorBlock([self tokenError]);
           } else {
               [self.requestParam addEntriesFromDictionary:@{
                                                             @"token": token
                                                             }];
               requestBlock();
           }
       }];
    }
}

- (NSError *)tokenError
{
    NSError *tokenError = [[NSError alloc] initWithDomain:@"token 获取失败" code:101 userInfo:nil];
    return tokenError;
}

- (NSMutableDictionary *)requestParam
{
    if (!_requestParam) {
        _requestParam = [NSMutableDictionary dictionary];
    }
    return _requestParam;
}

@end
