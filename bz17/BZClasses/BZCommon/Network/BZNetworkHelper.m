//
//  BZNetworkHelper.m
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZNetworkHelper.h"
#import <HYBNetworking.h>
#import "MJExtension.h"
#import "BZTestModel.h"

static BZNetworkHelper *workHelper = nil;

const static NSString *kErrorCode = @"error_code";
const static NSString *kErrorMessage = @"error_message";
const static NSString *kResultKey = @"key";

NSString *baseUrl = @"http://stone.vip.natapp.cn";

@implementation BZNetworkHelper

+ (instancetype)shareNetworkHelper
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        workHelper = [[BZNetworkHelper alloc] init];
    });
    return workHelper;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self helperConfig];
    }
    return self;
}

- (void)helperConfig
{
    [HYBNetworking enableInterfaceDebug:true];
    [HYBNetworking setTimeout:30.f];
    [HYBNetworking obtainDataFromLocalWhenNetworkUnconnected:YES];
    [HYBNetworking updateBaseUrl:baseUrl];
    [HYBNetworking configRequestType:kHYBRequestTypeJSON responseType:kHYBResponseTypeData shouldAutoEncodeUrl:false callbackOnCancelRequest:true];
}

- (NSURLSessionTask *)getWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(OnCompleteBlock)onComplete
                             fail:(OnErrorBlock)onError
                        modelName:(NSString *)modelName
{
    @weakify(self);
    HYBURLSessionTask *task = [HYBNetworking getWithUrl:url refreshCache:refreshCache params:params success:^(id response) {
        @strongify(self);
        [self successRequest:response modelName:modelName onComplete:onComplete error:onError];
    } fail:^(NSError *error) {
        BLOCK_EXEC(onError,error);
    }];
    return task;
}

- (NSURLSessionTask *)postWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(OnCompleteBlock)onComplete
                             fail:(OnErrorBlock)onError
                        modelName:(NSString *)modelName
{
    @weakify(self);
    return [HYBNetworking postWithUrl:url refreshCache:refreshCache params:params success:^(id response) {
        @strongify(self);
        [self successRequest:response modelName:modelName onComplete:onComplete error:onError];
    } fail:^(NSError *error) {
        BLOCK_EXEC(onError,error);
    }];
}

- (void)successRequest:(id)result modelName:(NSString *)modelName onComplete:(OnCompleteBlock)onComplete error:(OnErrorBlock)errorBlock
{
    if (![result isKindOfClass:[NSDictionary class]]) {
        BLOCK_EXEC(errorBlock,[NSError errorWithDomain:@"网络数据错误" code:101 userInfo:nil]);
        return;
    }
    NSMutableDictionary *mutablDict = [NSMutableDictionary dictionaryWithDictionary:result];
    PYJSONLog(@"resultDict --> %@\n",mutablDict);
    if (mutablDict[kErrorCode]) {
        // 表示接口返回失败
        NSError *error = [NSError errorWithDomain:mutablDict[kErrorMessage] code:101 userInfo:nil];
        errorBlock(error);
        return;
    }
    
    if (!modelName.length) {
        BLOCK_EXEC(onComplete, mutablDict);
        return;
    }
    
    id model = [[NSClassFromString(modelName) alloc] init];
    if (!model) {
        BLOCK_EXEC(onComplete, mutablDict);
        return;
    }
    
    model = [NSClassFromString(modelName) mj_objectWithKeyValues:mutablDict];
    BLOCK_EXEC(onComplete,model);
}



@end
