//
//  BZNetworkHelper.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *baseUrl;

@interface BZNetworkHelper : NSObject

+ (instancetype)shareNetworkHelper;

- (NSURLSessionTask *)getWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(OnCompleteBlock)onComplete
                             fail:(OnErrorBlock)onError
                        modelName:(NSString *)modelName;

- (NSURLSessionTask *)postWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(OnCompleteBlock)onComplete
                             fail:(OnErrorBlock)onError
                        modelName:(NSString *)modelName;

@end
