//
//  BZBaseRequest.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, BZHttpType) {
    BZHttpTypePOST,
    BZHttpTypeGET
};

@interface BZBaseRequest : NSObject

@property (nonatomic, strong) NSMutableDictionary *requestParam;
@property (nonatomic, copy)   OnCompleteBlock completeBlock;
@property (nonatomic, copy)   OnErrorBlock    errorBlock;
@property (nonatomic, copy)   NSString *methodName;

- (NSString *)url;

- (BZHttpType)httpType;

- (void)sendRequest;

- (NSString *)modelName;

@end
