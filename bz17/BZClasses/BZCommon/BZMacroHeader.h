//
//  BZMacroHeader.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#ifndef BZMacroHeader_h
#define BZMacroHeader_h
//安全调用block的方式
#define BLOCK_EXEC(block, ...) if (block) { block(__VA_ARGS__); };

// 系统参数
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define BZDefaultImageStr @""
#define BZDefaultImage [UIImage imageNamed:BZDefaultImageStr]

#define BZFont(f) [UIFont systemFontOfSize:(f)]

// block
#pragma mark - network使用
typedef void(^OnCompleteBlock)(id result);
typedef void(^OnErrorBlock)(id error);

#endif /* BZMacroHeader_h */

