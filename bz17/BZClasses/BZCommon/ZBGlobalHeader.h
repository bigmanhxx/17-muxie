//
//  ZBGlobalHeader.h
//  bz17
//
//  Created by bigMan on 16/6/27.
//  Copyright © 2016年 17. All rights reserved.
//

#ifndef ZBGlobalHeader_h
#define ZBGlobalHeader_h

#define FIRST_OPEN_DATE @"first_open_date"

// 存放个人信息key
#define kPersonKey @"personKey"

#endif /* ZBGlobalHeader_h */
