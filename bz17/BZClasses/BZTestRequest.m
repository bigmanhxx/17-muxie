//
//  BZTestRequest.m
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTestRequest.h"

@implementation BZTestRequest

- (NSString *)url
{
    return @"/user/login";
}

- (BZHttpType)httpType
{
    return BZHttpTypePOST;
}

- (NSString *)modelName
{
    return @"BZTestModel";
}

@end
