//
//  BZTestCollectionView.m
//  bz17
//
//  Created by yunhe.lin on 16/9/23.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTestCollectionView.h"

@implementation BZTestCollectionView

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.viewHeight = self.contentSize.height;
}

@end
