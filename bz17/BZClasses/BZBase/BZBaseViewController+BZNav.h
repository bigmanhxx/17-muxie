//
//  BZBaseViewController+BZNav.h
//  bz17
//
//  Created by bigMan on 16/6/28.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseViewController.h"

@interface BZBaseViewController (BZNav)

- (void)pushViewController:(NSString *)viewController With:(NSDictionary *)param animated:(BOOL)animated;

@end
