//
//  BZBaseViewController.m
//  bz17
//
//  Created by bigMan on 16/6/28.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseViewController.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"

@implementation BZBaseViewController

- (void)setParam:(NSDictionary *)param
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].systemVersion.floatValue < 10.0) {
        [self.navigationController.navigationBar py_setBackgroundColor:[UIColor whiteColor]];
    } else {
        [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
    }
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar.superview bringSubviewToFront:self.navigationController.navigationBar];
    if (self.navigationController.viewControllers.count > 1) {
        [self py_addLeftNavigationItem:[UIImage imageNamed:@"back_icon"] title:@"返回" observer:self action:@selector(popViewController)];
    }
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if ([self respondsToSelector:@selector(py_addTitle:)]) {
        [self py_addTitle:self.title];
    }
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - HUB

- (void)showSimpleHUD:(NSString *)tip afterDelay:(NSTimeInterval)delay
{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD setInfoImage:nil];
    [SVProgressHUD showInfoWithStatus:tip];
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
    });
}

- (void)showHUB
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hiddenHUB
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
