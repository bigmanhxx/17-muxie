//
//  BZBaseWebViewController+BZJSAction.m
//  bz17
//
//  Created by yunhe.lin on 16/8/24.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseWebViewController+BZJSAction.h"
#import "BZSystemManager.h"

NSString *kLoginChangeNotification = @"LoginChangeNotification";
NSString *kLoginOutNotification = @"LoginOutNotification";
@implementation BZBaseWebViewController (BZJSAction)

- (void)nativeLogin:(NSArray *)params
{
    [self popViewController];
    if (params.count == 0) return;
    NSString *isNeedLogin = [NSString stringWithFormat:@"%@", params[0]];
    if ([isNeedLogin integerValue] != 0) { // 是否需要登录
        return;
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personKey"];
    [BZSystemManager pushViewController:@"BZLoginViewController" With:@{} animated:YES]; // 跳转登录
}

- (void)loginOut:(NSArray *)params
{
    NSLog(@"reove ----   >>> %@", params);
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personKey"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginOutNotification object:nil];
}

@end
