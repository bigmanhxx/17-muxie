//
//  BZBaseWebViewController.h
//  bz17
//
//  Created by bigMan on 16/6/24.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseViewController.h"
#import "UIViewController+PYNavigationItem.h"


@interface BZBaseWebViewController : BZBaseViewController

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, copy)   NSString *url;
@property (nonatomic, copy) void(^reloadBlock)();

- (instancetype)initWithURLString:(NSString *)url;
    
    

@end
