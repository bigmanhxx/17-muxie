//
//  BZBaseWebViewController.m
//  bz17
//
//  Created by bigMan on 16/6/24.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseWebViewController.h"
#import "NJKWebViewProgressView.h"
#import "NJKWebViewProgress.h"
#import "UIViewController+PYNavigationItem.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "SVProgressHUD.h"
#import "BZBaseWebViewController+BZJSAction.h"
#import "BZTabarUrlManager.h"
#import "BZPopWindowHelper.h"
#import "BZHomeBottomModel.h"

#define kNeedCloseTitle (self.navigationController.viewControllers.count > 1 && self.webView.canGoBack)
#define kHasPopFlag (self.navigationController.viewControllers.count > 1)


@interface BZBaseWebViewController()<UIWebViewDelegate, JSExport, NJKWebViewProgressDelegate>

@property (nonatomic, copy)   NSString *cureentUrl;
@property (nonatomic, assign) BOOL goBackFlag;
@property (nonatomic, strong) JSContext *context;
@property (nonatomic, strong) NJKWebViewProgress *progress;
@property (nonatomic, strong) UIView *progressLineView;
@property (nonatomic, strong) NSMutableURLRequest *webRequest;

@end

@implementation BZBaseWebViewController


#pragma mark - lifyCyle

- (instancetype)initWithURLString:(NSString *)url
{
    if (self = [super init]) {
        self.url = [self addChannel:url];
    }
    return self;
}

- (void)configureAppearance{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [UINavigationBar appearance].barTintColor = [UIColor whiteColor];
    [UINavigationBar appearance].barStyle  = UIBarStyleBlack;
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    
    [UISegmentedControl appearance].tintColor = [UIColor whiteColor];
}

- (void)setParam:(NSDictionary *)param
{
    self.url = [self addChannel:param[@"url"]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginChangeReload) name:kLoginChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginOutReload) name:kLoginOutNotification object:nil];
    [self initView];
    @weakify(self);
    self.reloadBlock = ^{
        @strongify(self);
        [self.webView reload];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.webView loadRequest:self.webRequest];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginOutNotification object:nil];
}

- (void)initView
{
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.webView];
    NSURL *webUrl = [NSURL URLWithString:self.url];
    self.webRequest = [NSMutableURLRequest requestWithURL:webUrl cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:180];
    [self.webView addSubview:self.progressLineView];
}

- (void)popViewController
{
    if (self.webView.canGoBack) {
        [self.webView goBack];
        self.goBackFlag = YES;
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshLeftItems
{
    [self py_addLeftNavigationItem:[UIImage imageNamed:@"back_icon"] title:@"返回" observer:self action:@selector(popViewController) needClose:kNeedCloseTitle];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *url = request.URL.absoluteString;
    if ([url py_hasContainStr:@"about:blank"]) {
        return NO;
    }
    BZHomeBottomModel *bottomModel = [BZSystemManager homeBottomModel];
    NSString *huzhuHttpUrl = [request.URL.absoluteString componentsSeparatedByString:@"?"][0];
    if (![huzhuHttpUrl py_hasContainStr:bottomModel.domain]) {
        if ([url py_hasContainStr:@"login"]) {
            
        } else {
            if (self.navigationController.viewControllers.count == 1 && [self validUrl:request.URL.absoluteString]) {
                [self pushViewController:@"BZBaseWebViewController" With:@{
                                                                           @"url": [request.URL.absoluteString componentsSeparatedByString:@"?"][0]
                                                                           }animated:YES];
                return NO;
            } else {
                return YES;
            }
        }
    }
    if (![self hasChannel:url]) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[self addChannel:url]]];
        [self.webView loadRequest:request];
        return NO;
    }
    if (![request.URL.absoluteString isEqualToString:self.url]) {
        BOOL changeTabbar = [BZTabarUrlManager filterUrlForRoot:request.URL.absoluteString];
        if (changeTabbar) {
            return NO;
        }
    }
    
    if (self.goBackFlag) {
        self.goBackFlag = NO;
        return YES;
    }
    
    if ([request.URL.absoluteString py_hasContainStr:@"appentry"]) {
        return YES;
    }
    
    if ([request.URL.absoluteString py_hasContainStr:@"login"]) {
        if ([BZSystemManager showUpdateFlag]) {
            [[BZPopWindowHelper shareInstance] showPopWindow:BZPopWindowTypeSystem withBlock:^(id param) {
                NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@",
                                 @"com.muxie.huzhu"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            }];
            return NO;
        }
        [self pushViewController:@"BZLoginViewController" With:nil animated:YES];
        return NO;
    }
    
    if (self.navigationController.viewControllers.count == 1 && [self validUrl:request.URL.absoluteString]) {
        [self pushViewController:@"BZBaseWebViewController" With:@{
                                                                   @"url": [request.URL.absoluteString componentsSeparatedByString:@"?"][0]
                                                                   }animated:YES];
        return NO;
    }
    
    if ([request isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest *mutableRequest = (NSMutableURLRequest *)request;
        if ([self needAddPersonKey]) {
            return [self addPersonKeyParam:(NSMutableURLRequest *)request];
        } else {
            if (request.allHTTPHeaderFields[@"personKey"].length > 0) return [self removePersonkey:mutableRequest];
        }
    }
    
    return YES;
}

- (BOOL)removePersonkey:(NSMutableURLRequest *)request
{
    [request setValue:@"" forHTTPHeaderField:@"personKey"];
    [self.webView loadRequest:request];
    return NO;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self refreshLeftItems];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self refreshLeftItems];
    [self stepContext];
    [self hiddenHUB];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hiddenHUB];
}

#pragma mark - NJKWebViewProgressDelegate

- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    if (progress < 1) {
        self.progressLineView.hidden = NO;
    }
    [UIView animateWithDuration:0.3 animations:^{
       self.progressLineView.viewWidth = SCREEN_WIDTH * progress;
    } completion:^(BOOL finished) {
        if (progress >= 1) {
            self.progressLineView.hidden = YES;
        }
    }];
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.webView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - (kHasPopFlag ? 64  : 64 + 49))];
}

#pragma mark - event handler

- (void)loginChangeReload
{
//    if (kHasPopFlag) {
//        [self popViewController];
//    }
//    [self.webView reload];
}

- (void)loginOutReload
{
//    if (self.navigationController.viewControllers.count <= 1) {
//        [self.webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
//    } else {
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
}

#pragma mark - private method

- (BOOL)needAddPersonKey
{
    if ([BZSystemManager userLoginStatu] == BZLoginStatuLoginIn) {
        return YES;
    }
    return NO;
}

- (BOOL)addPersonKeyParam:(NSMutableURLRequest *)mutableRequest
{
    NSString *personKey = mutableRequest.allHTTPHeaderFields[@"personKey"];
    if (personKey.length > 0) {
        return YES;
    }
    [mutableRequest setValue:[BZSystemManager personKey] forHTTPHeaderField:@"personKey"];
    [self.webView loadRequest:mutableRequest];
    return NO;
}

- (BOOL)validUrl:(NSString *)url
{
    if (self.cureentUrl == nil) {
        self.cureentUrl = url;
        return NO;
    }
    if ([self.cureentUrl isEqualToString:url]) {
        return NO;
    }
    return YES;
}

- (NSString *)addChannel:(NSString *)url
{
    if ([url py_hasContainStr:@"channel"]) {
        return url;
    }
    if ([url py_hasContainStr:@"?"]) {
        return [NSString stringWithFormat:@"%@&channel=ios", url];
    }
    return [NSString stringWithFormat:@"%@?channel=ios", url];
}

- (BOOL)hasChannel:(NSString *)url
{
    return [url py_hasContainStr:@"channel"];
}


- (void)stepContext
{
    self.context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    /*!
     *  key : jsName value : handler
     */
    NSDictionary *jsDict = @{@"nativeLogin": ^(){[self nativeLogin:[JSContext currentArguments]];},
                             @"logout": ^(){[self loginOut:[JSContext currentArguments]];}};
    [jsDict.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        self.context[obj] = jsDict[obj];
    }];
    
    self.context.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        //比如把js中的方法名改掉，OC找不到相应方法，这里就会打印异常信息
        NSLog(@"异常信息：%@", exceptionValue);
    };
}

#pragma mark - accessors method

- (UIWebView *)webView {
	if(_webView == nil) {
		_webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.f - 49.f)];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.contentMode = UIViewContentModeRedraw;
        _webView.opaque = YES;
        _webView.delegate = self;
        _webView.delegate = self.progress;
        [_webView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.backgroundColor = [UIColor whiteColor];
        }];
	}
	return _webView;
}

- (NJKWebViewProgress *)progress {
	if(_progress == nil) {
		_progress = [[NJKWebViewProgress alloc] init];
        _progress.webViewProxyDelegate = self;
        _progress.progressDelegate = self;
	}
	return _progress;
}

- (UIView *)progressLineView {
	if(_progressLineView == nil) {
		_progressLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 2.0)];
        _progressLineView.backgroundColor = [UIColor greenColor];
	}
	return _progressLineView;
}

@end
