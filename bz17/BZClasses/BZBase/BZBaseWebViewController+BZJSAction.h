//
//  BZBaseWebViewController+BZJSAction.h
//  bz17
//
//  Created by yunhe.lin on 16/8/24.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseWebViewController.h"

FOUNDATION_EXTERN NSString *kLoginChangeNotification;
FOUNDATION_EXTERN NSString *kLoginOutNotification;

@interface BZBaseWebViewController (BZJSAction)

- (void)nativeLogin:(NSArray *)params;

- (void)loginOut:(NSArray *)params;

@end
