//
//  BZBaseViewController+BZNav.m
//  bz17
//
//  Created by bigMan on 16/6/28.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZBaseViewController+BZNav.h"
#import "BZSystemManager.h"

@implementation BZBaseViewController (BZNav)

- (void)pushViewController:(NSString *)viewController With:(NSDictionary *)param animated:(BOOL)animated
{
    BZBaseViewController *vc = [[NSClassFromString(viewController) alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    if (![vc respondsToSelector:@selector(setParam:)]) {
        return;
    }
    [BZSystemManager updateNowNavigation:self.navigationController];// 更新当前显示的导航栏
    [vc performSelector:@selector(setParam:) withObject:param];
    [self.navigationController pushViewController:vc animated:animated];
}

@end
