//
//  BZBaseViewController.h
//  bz17
//
//  Created by bigMan on 16/6/28.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+PYNavigationItem.h"

@interface BZBaseViewController : UIViewController

- (void)setParam:(NSDictionary *)param;

- (void)popViewController;

#pragma mark - HUB
- (void)showSimpleHUD:(NSString *)tip afterDelay:(NSTimeInterval)delay;

- (void)showHUB;

- (void)hiddenHUB;
@end
