//
//  BZTestModel.m
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import "BZTestModel.h"
#import "BZTestTwoModel.h"
#import "MJExtension.h"
#import <objc/runtime.h>


@implementation BZTestModel

+ (void)load
{
    [BZTestModel mj_setupObjectClassInArray:^NSDictionary *{
        return @{
                 @"tests": @"BZTestTwoModel"
                 };
    }];
}
//
//- (void)encodeWithCoder:(NSCoder *)aCoder
//{
//    unsigned int count = 0;
//    Ivar *ivars = class_copyIvarList([self class], &count);
//    
//    for (int i = 0; i<count; i++) {
//        // 取出i位置对应的成员变量
//        Ivar ivar = ivars[i];
//        // 查看成员变量
//        const char *name = ivar_getName(ivar);
//        // 归档
//        NSString *key = [NSString stringWithUTF8String:name];
//        id value = [self valueForKey:key];
//        if (!value) {
//            continue;
//        }
//        [aCoder encodeObject:value forKey:key];
//    }
//}
//
//- (instancetype)initWithCoder:(NSCoder *)aDecoder
//{
//    if (self = [self init]) {
//        
//        unsigned int count = 0;
//        Ivar *ivars = class_copyIvarList([self class], &count);
//        
//        for (int i = 0; i<count; i++) {
//            // 取出i位置对应的成员变量
//            Ivar ivar = ivars[i];
//            
//            // 查看成员变量
//            const char *name = ivar_getName(ivar);
//            
//            // 归档
//            NSString *key = [NSString stringWithUTF8String:name];
//            id value = [aDecoder decodeObjectForKey:key];
//            
//            // 设置到成员变量身上
//            [self setValue:value forKey:key];
//        }
//        
//        free(ivars);
//    }
//    return self;
//}


@end
