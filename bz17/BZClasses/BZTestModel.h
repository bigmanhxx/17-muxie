//
//  BZTestModel.h
//  bz17
//
//  Created by bigMan on 16/6/26.
//  Copyright © 2016年 17. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BZTestTwoModel;

@interface BZTestModel : NSObject

@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, copy)   NSString *msg;
@property (nonatomic, copy)   NSString *obj;
//@property (nonatomic, strong) NSMutableArray *tests;

@end
