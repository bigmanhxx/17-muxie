//
//  AppDelegate.m
//  bz17
//
//  Created by bigMan on 16/6/23.
//  Copyright © 2016年 17. All rights reserved.
//

#import "AppDelegate.h"
#import "BZBaseWebViewController.h"
#import "BZMainHomeViewController.h"
#import "BZSystemManager.h"
#import "BZNavigationController.h"
#import "BZShareManager.h"
#import "WXApi.h"
#import <ShareSDK/ShareSDK.h>
#import <Bugly/Bugly.h>
#import "BZPushHelper.h"
#import "JPUSHService.h"
#import "JPEngine.h"
#import "BZTestModel.h"
#import <UMMobClick/MobClick.h>
#import <UMMobClick/MobClickGameAnalytics.h>
#import <UMMobClick/MobClickSocialAnalytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window.backgroundColor = [UIColor whiteColor];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [[BZNavigationController alloc] initWithRootViewController:[[BZMainHomeViewController alloc] init]];
    [BZSystemManager updateNowNavigation:self.window.rootViewController];
    [self.window makeKeyAndVisible];
    [BZSystemManager stupGuideView:(BZMainHomeViewController *)self.window.rootViewController];
    [[BZShareManager shareInstance] shareSteup];
    [[BZPushHelper instance] steupPushHeler:launchOptions];
    [BZSystemManager updateVersion];
    
    UMConfigInstance.appKey = @"578c7e10e0f55add44001004";
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [JPUSHService setBadge:0];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
