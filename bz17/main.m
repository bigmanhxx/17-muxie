//
//  main.m
//  bz17
//
//  Created by bigMan on 16/6/23.
//  Copyright © 2016年 17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
